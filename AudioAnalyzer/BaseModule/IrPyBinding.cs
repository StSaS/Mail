﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using static AudioAnalyzer.Program;
using System.Globalization;

namespace AudioAnalyzer
{
    class IrPyBinding
    {
        ScriptEngine engine;
        ScriptScope scope;
        Form1 form;
       
        string databaseName;
        string pathPython;

        public IrPyBinding( string DB, Form1 form1 = null)
        {
            engine = Python.CreateEngine();
            var paths = engine.GetSearchPaths();
            paths.Add(@"..\Lib");
            engine.SetSearchPaths(paths);
            scope = engine.CreateScope();
            databaseName = DB;
            form = form1;
            
        }

        public bool startTeachingPython() // запустить .Py для обучения
        {
            return true;
        }

        public bool startEducate()
        {
            Process p = new Process();
            startPython(@"NeuroNet.py", p);

            return true;
        }

        public bool sendValidate() // берет из базы данных и отправляет в скрипт биндинг
        {
            // считать массив из базы данных и перадать в скрипт
            Process p = new Process();
            startPython(@"NeuroReceiverValidate.py", p);

            CBD cbd = new CBD(databaseName);
            Music music;
            int result = cbd.getCountTrack();

            sendSize(result, ref engine, ref scope); // отправляется количество данных
            //string output = p.StandardOutput.ReadToEnd();

            for (int i = 1; i <= result; i++)
            {
                music = cbd.getTrackFromID(i); // проверить, что получили трек , music.id != 0
                double[] message;

                Statistic.prescaler(ref music.metrics, out message,50); // сокращает данные


                sendVector(ref message, music.mark, ref engine, ref scope);// добавить параметр оценку

            }



            return true;
        }

        public bool sendAllData() // берет из базы данных и отправляет в скрипт биндинг
        {
            // считать массив из базы данных и перадать в скрипт
            Process p = new Process();
            startPython(@"NeuroReceiver.py", p);
            
            CBD cbd = new CBD(databaseName);
            cbd.CreateIndexes();

            Music music = new Music();
            int result = cbd.getCountTrack();
            
            sendSize( result, ref engine, ref scope); // отправляется количество данных
            //string output = p.StandardOutput.ReadToEnd();
            

            for (int i = 1; i <= result; i++)
            {
                music = cbd.getTrackFromID(i); // проверить, что получили трек , music.id != 0
                form.getListView().BeginInvoke(new InvokeDelegate(delegate ()
                {
                    form.getListView().Items.Add((new ListViewItem(form.getListView().Items.Count.ToString() + ". " + DateTime.Now.ToString() + " - " + music.name)));
                }));
                if (music.id == 0) continue;

               
                double[] message;
                
                Statistic.prescaler(ref music.metrics, out message,50); // сокращает данные

                sendCSV(ref message, music.mark, music.name);
                sendVector(ref message, music.mark, ref engine, ref scope);// добавить параметр оценку
                
            }

           

            return true;
        }

        public static void sendCSV(ref double[] data, long mark, string name )
        {
            using (var w = new StreamWriter("metrics1000.csv",true)) // true == append
            {
                var array = new StringBuilder();

                for (int i=0; i< data.Length; i++)
                {
                    
                   array.Append( data[i].ToString("F", CultureInfo.CreateSpecificCulture("en-CA")) + ",");
                   
                }

                var newLine = string.Format("{0}{1},{2}", array, mark, name);
                //csv.AppendLine(newLine);
                w.WriteLine(newLine);
                //after your loop
                w.Flush();
                //File.AppendAllText("metrics1000.csv", csv.ToString());
            }
        }
        public static bool sendVector( ref  double[] data, long mark,ref ScriptEngine engine, ref ScriptScope scope)
        {
            try
            {
                // нормализовать данные
                scope.SetVariable("doubleArray", data);
                scope.SetVariable("mark", (double)mark);
                // добавить оценку
                engine.ExecuteFile(@"..\IronPythonNeuroNet\IronPythonBinding.py", scope);
            }
            catch (Exception exception) // показывает исключения внутри
            {
                Console.WriteLine(exception.Message);
                
            }

            return true;
        }

        public static bool sendSize( int size, ref ScriptEngine engine, ref ScriptScope scope)
        {
            try
            {
                scope.SetVariable("size", size);
                engine.ExecuteFile(@"..\IronPythonNeuroNet\IPySendSize.py", scope);
            }
            catch (Exception exception) // показывает исключения внутри
            {
                Console.WriteLine(exception.Message);

            }
            return true;
        }

        public static void startPython( string filePython, Process p )
        {
            

            p.StartInfo = new ProcessStartInfo()
            {
                FileName = @"C:\Python27\python.exe",//cmd is full path to python.exe
                Arguments = filePython,//args is path to .py file and any cmd line args
                WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory,
                RedirectStandardOutput = false,
                UseShellExecute = false,
                CreateNoWindow = false,
                //Verb = "runas",
                RedirectStandardInput = false,
                RedirectStandardError = false
            };

            p.Start();
            
           
        }

    }
}
