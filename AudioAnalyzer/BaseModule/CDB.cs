﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data.Common;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;

namespace AudioAnalyzer
{
    class CBD
    {
        
        private string Database;
        private SQLiteConnection permConnection;

        public CBD(string databaseName, bool permMode = false)
        {
            Database = databaseName;

            try
            {
                if (!File.Exists(databaseName)) // если нет
                {
                    SQLiteConnection.CreateFile(databaseName);
                }
            }
            catch (System.IO.IOException ex)
            {

            }
            if (permMode)
            {   
                this.permConnection = new SQLiteConnection(string.Format("Data Source={0};", Database));
                this.permConnection.Open();
            }
        }

        //получить количество треков
        public int getCountTrack()
        {
            int result = 0;
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0};", Database)))
                {
                    connection.Open(); 
                    using (SQLiteCommand command = new SQLiteCommand(connection)) 
                    {
                        
                        command.CommandText = "SELECT COUNT(*) FROM 'MUSIC';";

                        //SQLiteDataReader reader = command.ExecuteReader();
                        result = Convert.ToInt32(command.ExecuteScalar());
                       
                    }
                }
            }
            catch (SQLiteException ex)
            {

            }
            return result;
        }

        // переделать для RNN with fragment
        // искать по номеру фрагмента и имени
        // сгруппировать предварительно по названию и отсортировать по номеру
        public void CreateIndexes()
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0};", Database)))
                {
                    connection.Open(); 
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "DROP INDEX IF EXISTS 'index_track';" +
                            //"DROP INDEX IF EXISTS 'index_frag';" +
                            "CREATE INDEX 'index_track' ON 'MUSIC'('track');";
                           // "CREATE INDEX 'index_frag' ON 'MUSIC'('frag');";
                        SQLiteDataReader reader = command.ExecuteReader();
                    }

                }
            }

            catch ( SQLiteException ex)
            {

            }
        }

        public Music getTrackFromID( int id = 1) // если music.id == 0, то нет ответа
        {
            Music music = new Music();
            music.metrics = new double[10240];
            music.id = 0;

            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0};", Database)))
                {
                    connection.Open(); 
                    
                    using (SQLiteCommand command = new SQLiteCommand(connection)) // выбрать по id
                    {
                       
                        
                       
                        command.CommandText = "SELECT * FROM 'MUSIC' order by track desc, frag asc;";
                        SQLiteDataReader reader = command.ExecuteReader();

                       

             

                        int indTrack = 1;
                        foreach (DbDataRecord record in reader)
                        {

                            if (indTrack < id)
                            {
                                indTrack++;
                                continue;
                            }

                            if (indTrack > id) return music;

                            music.id = (long)record["id"];
                            music.name = record["track"].ToString();

                            byte[] array = (byte[])record["param"];
                            FreqPacket fp = getPacket(ref array);
                            int k = 0;
                            foreach (double stat in fp.data)
                            {
                                music.metrics[k] = stat;
                                k++;
                            }
                            music.mark = (long)record["mark"];

                            return music;
                        }
                    }
                    connection.Close();
                }
            }
            catch (SQLiteException ex)
            {

            }

            return music;
        }

        public static byte[] getBytes(ref FreqPacket FP)
        {
            int size = Marshal.SizeOf(FP);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(FP, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        } 

        public static FreqPacket getPacket(ref byte[] arrayF)
        {
            FreqPacket fp = new FreqPacket();

            int size = Marshal.SizeOf(fp);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arrayF, 0, ptr, size);

            fp = (FreqPacket)Marshal.PtrToStructure(ptr, fp.GetType());
            
            Marshal.FreeHGlobal(ptr);

            return fp;
        }


        public void add(string trackName, ref FreqPacket arrayF)
        {
            byte[] array = getBytes(ref arrayF); // проверка Marshalling
                                                 //FreqPacket fp = getPacket(ref array);
            int mark = 0; // 0 - не определена цена песни
            

            DirectoryInfo d = new DirectoryInfo(Path.GetDirectoryName(trackName));
            string markString = d.Name;

            if (!Int32.TryParse(markString, out mark))
                mark = 2; // цена не определена, то 2

            // Name directory 4 - mark = 4, e.t.c.
            //mark--; // в Python оценки с нуля - оценки 0-3
            
            try // замедление на 30%
            {
                using (SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0};", Database)))
                {
                    connection.Open();
                   

                    using (SQLiteTransaction transaction = connection.BeginTransaction()) //    замедление на 30 %
                    {
                        using (SQLiteCommand command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS 'MUSIC' (id INTEGER PRIMARY KEY AUTOINCREMENT, track TEXT, param BLOB, mark INTEGER);", connection))
                        { // сделать index у трек ; если track существует больше не добавлять unique
                            // для нейронной сети не важно, если повториться
                           
                         
                            command.ExecuteNonQuery();
                            
                            command.CommandText = "INSERT INTO 'MUSIC' ('track','param','mark') VALUES (@trackName,@array,@mark)";
                            command.Parameters.Add("@array", DbType.Binary, array.Length).Value = array;
                            command.Parameters.Add("@trackName", DbType.String, trackName.Length * sizeof(char)).Value = trackName;
                            command.Parameters.Add("@mark", DbType.Int32, sizeof(int)).Value = mark;
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }

                    connection.Close();
                }

            }
            catch (SQLiteException ex)
            {
              if (ex.ResultCode.Equals(SQLiteErrorCode.Busy))
                    Console.WriteLine("Database is locked by another process!");
            }
        }

        public void addFrag(string trackName, int fragNumb, ref FreqPacket arrayF)
        {
            byte[] array = getBytes(ref arrayF); // проверка Marshalling
                                                 //FreqPacket fp = getPacket(ref array);
            int mark = 0; // 0 - не определена цена песни
           

            DirectoryInfo d = new DirectoryInfo(Path.GetDirectoryName(trackName));
            string markString = d.Name;

            if (!Int32.TryParse(markString, out mark))
                mark = 2; // цена не определена, то 2

            // Name directory 4 - mark = 4, e.t.c.
            //mark--; // в Python оценки с нуля - оценки 0-3

            try // замедление на 30%
            {
                

                    using (SQLiteTransaction transaction = permConnection.BeginTransaction()) //    замедление на 30 %
                    {
                        using (SQLiteCommand command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS 'MUSIC' (id INTEGER PRIMARY KEY AUTOINCREMENT, track TEXT, param BLOB, mark INTEGER, frag INTEGER );", this.permConnection))
                        { // сделать index у трек ; если track существует больше не добавлять unique
                          // для нейронной сети не важно, если повториться
                          
                            command.ExecuteNonQuery();

                            command.CommandText = "INSERT INTO 'MUSIC' ('track','param','mark','frag') VALUES (@trackName,@array,@mark,@frag)";
                            command.Parameters.Add("@array", DbType.Binary, array.Length).Value = array;
                            command.Parameters.Add("@trackName", DbType.String, trackName.Length * sizeof(char)).Value = trackName;
                            command.Parameters.Add("@mark", DbType.Int32, sizeof(int)).Value = mark;
                            command.Parameters.Add("@frag", DbType.Int32, sizeof(int)).Value = fragNumb;
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }

                   
                

            }
            catch (SQLiteException ex)
            {
                if (ex.ResultCode.Equals(SQLiteErrorCode.Busy))
                    Console.WriteLine("Database is locked by another process!");
            }

            
     
        }

        public void disconnect()
        {
            this.permConnection.Close();
        }



    }
}
