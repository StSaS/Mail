﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AudioAnalyzer
{
    public partial class Form1 : Form
    
    {
  

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

           
        }

        public ListView getListView()
        {
            return listView1;
        }
        public string GetDir()
        {
            return textBox1.Text;
        }

        FolderBrowserDialog fbd = new FolderBrowserDialog();
       
        private void button1_Click(object sender, EventArgs e)
        {

           if ( fbd.ShowDialog() == DialogResult.OK )
            {
                textBox1.Text = fbd.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.ProcessMetrics();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Program.Educate();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Program.Validate();
        }
    }
}
