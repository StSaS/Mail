﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Meta.Numerics;

namespace AudioAnalyzer // объединить все в одно инициализация через out массива
{
    class Statistic
    {
        int size;
       
       
        
        // перегрузить функции для интервалов

        public Statistic(int sizeFrame, string dbPath)
        {
            size = sizeFrame;
            databaseName = dbPath;
        }
        public double median(ref double[][] frame, int index) // аргумент это амплитуды одной частоты во времени
        {
            Array.Sort(frame[index]); 
            // frame.length - четное
            return (frame[index][size/2] + frame[index][size/2 - 1])/2;
        }

        public double medianN(ref double[][] frame, int index, int begin, int step) // аргумент это амплитуды одной частоты во времени
        {
            Array.Sort(frame[index], begin, step);


            return (frame[index][ step / 2 + begin] + frame[index][ step / 2 - 1 + begin]) / 2;
        }


        public double average(ref double[][] frame, int index) // median and average вывести в обобщающую
        {
            
            return frame[index].Average(); // избежать переполнение
        }

        public double averageN(ref double[][] frame, int index, int begin, int step) // median and average вывести в обобщающую
        {
            double sum = 0;

            for (int i = begin; i < begin + step; i++)
            {
                sum += frame[index][i] / step;
            }

            return sum;
            
        }

        public double stdev(ref double[][] frame, int index, double average) // average подается вместе
        {
            double Sum = 0;
            double n = frame[index].Length - 1;
            
            foreach (double x in frame[index])
            {
                double k = MoreMath.Sqr(x - average)/n;
                Sum += k; 
            }


            return Math.Pow(Sum,0.5);
        }

        public double stdevN(ref double[][] frame, int index, double average, int begin, int step) // average подается вместе
        {

            double Sum = 0;
            double n = step - 1;

            for (int i = begin; i < begin + step; i++)
            {
                double k = MoreMath.Sqr(frame[index][i] - average) / n;
                Sum += k;
            }


            return Math.Pow(Sum, 0.5);
        }

        public double skewness(ref double[][] frame, int index, double stdev, double average) // s - stdev 2
        {
            double Sum = 0;
            double n = frame[index].Length;
           
            foreach ( double x in frame[index]) // следить за переполнением
            {
                Sum += Math.Pow(((x - average)/stdev), 3);
            }

           
            return Sum/n;
        }

        public double skewnessN(ref double[][] frame, int index, double stdev, double average, int begin, int step) // s - stdev 2
        {
            double Sum = 0;
            double n = step;


            for (int i = begin; i < begin + step; i++)
            {
                double k = Math.Pow(((frame[index][i] - average) / stdev), 3);
                Sum += k;
            }


            return Sum / n;
        }



        public double kurtosis(ref double[][] frame, int index , double stdev, double average)
        {
            double Sum = 0;
            double n = frame[index].Length;
           
            foreach (double x in frame[index]) // следить за переполнением
            {
                Sum += Math.Pow(((x - average)/stdev), 4);
            }

            return Sum/n;
        }

        public double kurtosisN(ref double[][] frame, int index, double stdev, double average, int begin, int step)
        {
            double Sum = 0;
            double n = step;

            for (int i = begin; i < begin + step; i++)
            {
                double k = Math.Pow(((frame[index][i] - average) / stdev), 4);
                Sum += k;
            }

            return Sum / n;
        }

        public static bool prescaler( ref double[] source, out double[] dest, int scala = 10) // сократить частоты
        {
            dest = new double[2000/scala*5];
            for ( int i=0; i<2000/scala; i++) // по блокам частотам (берем 2000 только)
            {
                double s1, s2, s3, s4, s5;
                s1 = s2 = s3 = s4 = s5 = 0;
                for ( int g=0; g<scala; g++) // идем по блоку
                {
                    s1 += source[i * 5 * scala + g * 5];
                    s2 += source[i * 5 * scala + g * 5 + 1];
                    s3 += source[i * 5 * scala + g * 5 + 2];
                    s4 += source[i * 5 * scala + g * 5 + 3];
                    s5 += source[i * 5 * scala + g * 5 + 4];
                }
                dest[i * 5] = s1 / scala;
                dest[i * 5 + 1] = s2 / scala;
                dest[i * 5 + 2] = s3 / scala;
                dest[i * 5 + 3] = s4 / scala;
                dest[i * 5 + 4] = s5 / scala;
            }
           
            return true;
        }

        // сделать нормировку

        
    }
}
