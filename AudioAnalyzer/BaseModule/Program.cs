﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using NAudio;
using System.Runtime.InteropServices;
using System.Diagnostics;
//using Meta.Numerics;
using System.Data.SQLite;
using System.Data.Common;
using System.IO;

namespace AudioAnalyzer
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FreqPacket
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10240)]
        public double[] data;
    }

    

    public struct Music
    {
        public long id;
        public string name;
        public double[] metrics; // 200 * 5 metrics
        public long mark;
    }

   

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        /// 

       
        static Form1 form1 = new Form1();
        static string databaseName = @"cyber.db";
        static string databaseValidate = @"validate.db";
        public delegate void InvokeDelegate();
        static Object Lock = new Object();

        [STAThread] 
        static void Main()
        {
            Application.EnableVisualStyles();
           
           
            string dirName = @"C:\Users\DEXP G115\Dropbox\music";
            

           
            Application.Run(form1);           

        }

        // сделать кнопку validate, кторый по сокету связывается со скриптом Python validate и дает ответ по песни
        // дать папку с validate и ответ выводить - прогнозируемая оценка(класс)
        static public void Validate()
        {
            //добавить одну  базу validate без оценок (дефолтные) в нее метрики
            // и выслать на validate та ответ 
            

            if (Directory.Exists(form1.GetDir())) // dirName
            {
                string[] files = Directory.GetFiles(form1.GetDir());
                

                foreach (string s in files)
                {
                    
                   // добавление validate вариантов в базу
                   
                    try
                    {
                        
                            FuryeAnylyze FA = new FuryeAnylyze(databaseValidate);
                            FA.GetStat(s);

                            
                            form1.getListView().BeginInvoke(new InvokeDelegate(delegate ()
                            {
                                form1.getListView().Items.Add((new ListViewItem(form1.getListView().Items.Count.ToString() + ". " + DateTime.Now.ToString() + " - " + s)));
                            }));

                            Application.DoEvents();


                    }

                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);

                    }

                }

                IrPyBinding irpb = new IrPyBinding(databaseValidate);
                irpb.sendValidate();
                // использовать IrPyBinding, подправить(пересоздать) NeuroReciever - чтоб писал в файл validate.csv
                // затем запустить скрипт NeiroValidate, который будет читать из validate.csv и пишет в result.csv
                // вывести из result результаты и название трека
            }
        }
        static public void Educate()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state) {
                IrPyBinding irpb = new IrPyBinding(databaseName, form1);
                irpb.sendAllData();

                // irpb.startEducate() // запускается обучение скрипт
                // обучить сеть
                // по логике скрипт NeuroReciever должен запускать NeuroNet, который сохраняет сеть
                // но нужно получить ответ по сокету для контроля обучения
                // процент ошибок
            }));
            
        }
        static public void ProcessMetrics()
        {
            ThreadPool.SetMaxThreads(2, 2);
            int locker = 4;
            
            if (Directory.Exists(form1.GetDir())) // dirName
            {
                string[] files = Directory.GetFiles(form1.GetDir());
                // обьеденить с нескольких директорий
                //form1.getListView().co
                //Semaphore mtx = new Semaphore(4, 4);
            
                foreach (string s in files)
                {
                    while (locker <= 0)
                    {
                        //form1.Activate();
                        Application.DoEvents();
                    }
                    // сделать бесконечный пустой цикл по int , которую меняешь в локе
                    // чтобы не набивался пул
                    lock (Lock)
                    {
                        locker--;
                    }
                    try
                    {
                        //mtx.WaitOne();
                        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state)
                        {
                            object[] array = state as object[];
                            FuryeAnylyze FA = new FuryeAnylyze(array[0].ToString());
                            FA.GetStat(array[1].ToString());

                            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
                            // form1.getListView.ad

                            //вывод в listview



                            // BeginInvoke(new Action(() => textbox.Text += text));
                            //form1.getListView().Items.Add((new ListViewItem(array[1].ToString())));
                            form1.getListView().BeginInvoke(new InvokeDelegate(delegate ()
                           {
                               form1.getListView().Items.Add((new ListViewItem(form1.getListView().Items.Count.ToString() + ". " + DateTime.Now.ToString() + " - " + array[1].ToString())));
                           }));

                            //mtx.Release();
                            lock (Lock)
                            {
                                locker++;
                            }
                            // вывод из многих процессов в виджет
                            // Console.WriteLine(array[1].ToString());

                        }), new object[] { databaseName, s });
                    }
                   
                    catch (Exception exception) 
                    {
                        Console.WriteLine(exception.Message);

                    }
                
                    
                }

                


            
            }
        }
    }
}
