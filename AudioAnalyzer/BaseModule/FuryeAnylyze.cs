﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using System.Threading.Tasks;
using NAudio;
//using System.Numerics;
using System.Runtime.InteropServices;
using System.Data.SQLite;
using System.Data.Common;
using System.IO;
using NAudio.Wave;
using Meta.Numerics.SignalProcessing;
using Meta.Numerics;
using System.Globalization;

namespace AudioAnalyzer
{
    class FuryeAnylyze
    {
        private string pathDatabase;
        private byte[] _leftBuffer;
        short[] samples;

        public FuryeAnylyze(string databaseName)
        {
            pathDatabase = databaseName;
            
        }
        
        public static bool statForSampleN(ref double[][] data, string databaseName, string fileName, int step = 10)
        {
            Statistic stat = new Statistic(data[0].Length, databaseName);
            FreqPacket FP = new FreqPacket();
            CBD cbd = new CBD(databaseName,true); // постоянное соединение устанавливается
            FP.data = new double[10240];
            int k = 0;

            // по правилу 1/10 - берем только 1/10 случайно выбрав из интервала
            // нарезать на большее количество
            int sizeSample = 100; // целочисленное деление

            if (data[0].Length < 1000) return false;
            
            int beginer = 0;

            for (int index = 0; index < 10; index++)
            {
                beginer = index * sizeSample;
                k = 0;
                // Добавлять по фрагентам
                for (int i = 0; i < 2048; i++) // работа с часть массива // идем по частотам
                {
                    // 1/10 часть


                    double avg = stat.averageN(ref data, i, beginer, sizeSample);// для частоты k-той во времени
                    double stdev = stat.stdevN(ref data, i, avg, beginer, sizeSample);
                    double med = stat.medianN(ref data, i, beginer, sizeSample); // тяжелые функции
                    double skewness = stat.skewnessN(ref data, i, stdev, avg, beginer, sizeSample); // ТФ
                    double kurtosis = stat.kurtosisN(ref data, i, stdev, avg, beginer, sizeSample); // ТФ

                    FP.data[k] = avg;
                    FP.data[k + 1] = stdev;
                    FP.data[k + 2] = med;
                    FP.data[k + 3] = skewness;
                    FP.data[k + 4] = kurtosis;

                    k += 5;

                }



                cbd.addFrag(fileName,index+1, ref FP); // добавляет в базу статистику для трека
                                           // оптимизировать добавлени + новая схема со временем
                                           // добавлять группой или в отдельном потоке
                                           // создать список команд и потом добавить транзыкцией
                                           // либо передавать список FP
                                           // нужно меньше обращаться на запись
            }

            cbd.disconnect();

            return true;
        }
        public static bool statForSample(ref double[][] data, string databaseName, string fileName)
        {

            Statistic stat = new Statistic(data[0].Length, databaseName);
            FreqPacket FP = new FreqPacket();
            CBD cbd = new CBD(databaseName);
            FP.data = new double[10240];
            int k = 0;

                // разделить на интервалы
                // обсчитывать частоты на интервалах
                for (int i = 0; i < 2048; i++)
                {
                    
                    
                    double avg = stat.average(ref data, i);// для частоты k-той во времени
                    double stdev = stat.stdev(ref data, i, avg);
                    double med = stat.median(ref data, i); // тяжелые функции
                    double skewness = stat.skewness(ref data, i, stdev, avg); // ТФ
                    double kurtosis = stat.kurtosis(ref data, i, stdev, avg); // ТФ

                    FP.data[k] = avg;
                    FP.data[k + 1] = stdev;
                    FP.data[k + 2] = med;
                    FP.data[k + 3] = skewness;
                    FP.data[k + 4] = kurtosis;

                    k += 5;
                
                }

              
                cbd.add(fileName, ref FP); // добавляет в базу статистику для трека

                return true;
            
        }
        public void GetStat(string fileName)
        {
            // проверка на не null и mp3
            if (fileName == null)
                return;
            //создать исключение
            if (!Path.GetExtension(fileName).Equals(".mp3"))
                return;
            // создать исключение

            //Console.WriteLine(fileName);
            try
            {

                using (var reader = new Mp3FileReader(fileName))
                {
                    var pcmLength = (int)reader.Length;
                    _leftBuffer = new byte[pcmLength / 2];
                    var buffer = new byte[pcmLength];
                    var bytesRead = reader.Read(buffer, 0, pcmLength);

                    int index = 0;
                    for (int i = 0; i < bytesRead; i += 4)
                    {
                        _leftBuffer[index] = buffer[i];
                        index++;
                        _leftBuffer[index] = buffer[i + 1];
                        index++;
                    }

                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                return;
            }

            if (_leftBuffer.Length / 4096 < 500) return; // файлы больше 60 сек только

            var waveBuffer = new WaveBuffer(_leftBuffer);

            //FileInfo f = new FileInfo("A.txt"); // для 
            //StreamWriter w = f.AppendText(); //     проверки

            // записать файл для проверки
            /* for (int k = 0; k < 83; k++)
             {
                 string text = k.ToString() + "," + data[i][k].ToString();
                 w.WriteLine(text); //смотри Морзе.срр
             }*/

            // w.Flush();

            FourierTransformer ft = new FourierTransformer(4096);//разрешение по частоте 32768 надо == 0.74 с
           
           
            double [][] data = new double[2048][];
            for(int i=0; i<2048; i++)
            {
                int k = _leftBuffer.Length / (2*2048) - 1;
                data[i] = new double[k];
            }

            bool flag = true;
            double max = 0;
            int freq, time;

            for (int i = 0; i <( _leftBuffer.Length / (2*2048) - 1); i++)
            {
                // анализировать только первую минуту
                // i < 600 // проверить
                
                Complex[] data_interval = new Complex[4096];
                
                for (int k = 0; k < 4096; k++)
                {
                    if ((i * 2048 + 4096)>= _leftBuffer.Length / 2) //50%перекрытие
                    {
                        flag = false;
                        break;
                    }

                    //double G = Gausse(k,4096); // Функция окна Гаусса

                    //Complex comp = new Complex(waveBuffer.ShortBuffer[i * 2048 + k] * G, 0); // G - для окна Гаусса
                    Complex comp = new Complex(waveBuffer.ShortBuffer[i * 2048 + k], 0); // G - для окна Гаусса


                    data_interval[k] = comp;

                    flag = true;
                    
                }
                // i параметр время ; k - частота
                if (flag)
                {
                    Complex[] xt = ft.Transform(data_interval);
                    
                    //double[] xtd = new double[2048];
                    int k = 0;
    
                    foreach (Complex c in xt)
                    {
                        if (k == 2048) break;
                        //Math.Pow(MoreMath.Sqr(c.Re) + MoreMath.Sqr(c.Im), 0.5); // частоты магнитуды в момент времени
                        data[k][i] = Math.Pow(MoreMath.Sqr(c.Re) + MoreMath.Sqr(c.Im), 0.5)/2048; // 20log(y)

                       
                        k++;
                    }
                    
                    
                }
            }

           
            bool corret = statForSampleN(ref data, this.pathDatabase, fileName);
            
            
            // сократить параметры /k ratio about k = 10
            // data подать на Statistic - получим статистику для каждой частоты

            // data элементы с перекрытием
            //data элементы k суммировать и делить и вывести на 2048


            
        }

        static public double Gausse(double i, int framesize) // длина 4096 
        {
            double a = (framesize - 1.0 ) / 2;
            double t = (i - a) / (0.5 * a);
            t = t * t;

            return Math.Exp(-t / 2);
        }
    }
}
