package main

import (
	"net/http"
	"encoding/json"
	"encoding/xml"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"sort"
	"errors"

	"reflect"
	"strconv"
)

var (
	path = "dataset.xml"
)

type Seacher struct {
	
	Name   string
	
	About  string
	
}

const (
	CorrectAccessToken = "d41d8cd98f00b204e9800998ecf8427e"
)

func Sort(users []User, orderField string, orderBy string ) error {
	if (orderBy == "0") { 
		return nil
	}
	if (orderField == "Id") && (orderBy == "-1") { // по убыванию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Id > users[j].Id
		})
		return nil
	}
	if (orderField == "Id") && (orderBy == "1") { // по возрастанию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Id < users[j].Id
		})
		return nil
	}
	if (orderField == "Age") && (orderBy == "-1") { // по убыванию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Age > users[j].Age
		})
		return nil
	}

	if (orderField == "Age") && (orderBy == "1") { // по возрастанию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Age < users[j].Age
		})
		return nil
	}

	if (orderField == "Name") && (orderBy == "-1") { // по убыванию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Name > users[j].Name
		})
		return nil
	}

	if (orderField == "Name") && (orderBy == "1") { // по возрастанию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Name < users[j].Name
		})
		return nil
	}

	if (orderField == "") && (orderBy == "-1") { // по убыванию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Name > users[j].Name
		})
		return nil
	}

	if (orderField == "") && (orderBy == "1") { // по возрастанию
		sort.Slice(users[:], func(i, j int) bool {
			return users[i].Name < users[j].Name
		})
		return nil
	}

	return errors.New("wrong format orderField")

}

func SearchXml(name, about string, ofset, limit int) ([]User,error) {
	
	xmlFile, err := os.Open(path)
	//l := 0
	o := 0

	if err != nil {
		return nil, errors.New(err.Error())


	}
	
	defer xmlFile.Close()
	
	b, err := ioutil.ReadAll(xmlFile)

	if err != nil {
		return nil, errors.New(err.Error())
	}

	input := bytes.NewReader(b)

	decoder := xml.NewDecoder(input)

	users := make([]User, 0)

	var user User
	var first_name string
	var last_name string
	k:=0

	for {
		tok, tokenErr := decoder.Token()
		if tokenErr != nil && tokenErr != io.EOF {
			fmt.Println("error happend", tokenErr)
			return nil,errors.New(tokenErr.Error())
		} else if tokenErr == io.EOF {
			break
		}
		if tok == nil {
			fmt.Println("t is nil break")
		}
		switch tok := tok.(type) {
		case xml.StartElement:
			if tok.Name.Local == "last_name" {
				if err := decoder.DecodeElement(&last_name, &tok); err != nil {
					//fmt.Println("error happend", err)
					return nil,errors.New(err.Error())
				
				}
				k++
			}

			if tok.Name.Local == "first_name" {
				if err := decoder.DecodeElement(&first_name, &tok); err != nil {
					//fmt.Println("error happend", err)
					return nil,errors.New(err.Error())
				}
				k++
			}
				if tok.Name.Local == "gender" {
					if err := decoder.DecodeElement(&user.Gender, &tok); err != nil {
						//fmt.Println("error happend", err)
						return nil,errors.New(err.Error())
					}
					k++
				}
				if tok.Name.Local == "age" {
				if err := decoder.DecodeElement(&user.Age, &tok); err != nil {
					//fmt.Println("error happend", err)
					return nil,errors.New(err.Error())
				}
				k++
			}
				if tok.Name.Local == "about" {
					if err := decoder.DecodeElement(&user.About, &tok); err != nil {
						//fmt.Println("error happend", err)
						return nil,errors.New(err.Error())
					}	
					k++
				}
					if tok.Name.Local == "id" {
						if err := decoder.DecodeElement(&user.Id, &tok); err != nil {
							//fmt.Println("error happend", err)
							return nil,errors.New(err.Error())
						}	
						k++
					}
				if k==6 {

					if (ofset>o) {
						o++
						k = 0
						continue
					}

					if (limit == len(users)) {
						break
					}
					user.Name = first_name + " " + last_name
					// добавить условия для добавления
					if (name == " ") && (about=="") { 
						users = append(users, user) 
						
					} else if (name == " ") && (about !="") {
						if user.About == about {
							users = append(users, user) 
							
						} 
					} else if (name != " ") && (about =="") {
						if user.Name == name {
							users = append(users, user) 
						} 
					} else if (name != " ") && (about !="") {
						if (user.Name == name) && (user.About == about) {
							users = append(users, user) 
						} 
					}
					k=0
					o++ // сместилось на 1
				}
			}
		}

	return users,nil
}

func SearchServer(w http.ResponseWriter, r *http.Request) { // test_server in main - test
	
	SearchParam := r.URL.Query().Get("query") // { "Name": "firstname lastname", "About": "about"} | ""
	OrderParam := r.URL.Query().Get("order_field") // ("Id" | "Age" | "Name" | "" ) 
	OrderBy := r.URL.Query().Get("order_by")

	Limit, err := strconv.Atoi(r.URL.Query().Get("limit"))

	if err != nil {
		//fmt.Println("unexpected error: %v", err)
		errResp := SearchErrorResponse{}
		errResp.Error = err.Error()
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}


	Ofset, err := strconv.Atoi(r.URL.Query().Get("ofset"))

	if err != nil {
		//fmt.Println("unexpected error: %v", err)
		errResp := SearchErrorResponse{}
		errResp.Error = err.Error()
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}
	//обрабатывать ofset and limit
	//orderBy
	// 0 - не сортируем
	// -1 / 1 - знак

	at := r.Header.Get("AccessToken")

	if at != CorrectAccessToken {
		w.WriteHeader(http.StatusUnauthorized)
		io.WriteString(w,"")

		return
	}

	name := ""
	about := ""

	if SearchParam != "" {

	var tmpData interface{}
	err := json.Unmarshal([]byte(SearchParam), &tmpData)

	if err != nil {
		//fmt.Println("unexpected error: %v", err)
		errResp := SearchErrorResponse{}
		errResp.Error = err.Error()
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}

	if reflect.TypeOf(tmpData).Kind() != reflect.Map {
		errResp := SearchErrorResponse{}
		errResp.Error = "query is not map"
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}

	data := tmpData.(map[string]interface{})

	if reflect.TypeOf(data["first_name"]).Kind() != reflect.String {
		errResp := SearchErrorResponse{}
		errResp.Error = "first_name is not string"
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}
	if reflect.TypeOf(data["last_name"]).Kind() != reflect.String {
		errResp := SearchErrorResponse{}
		errResp.Error = "last_name is not string"
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}
	if reflect.TypeOf(data["about"]).Kind() != reflect.String {
		errResp := SearchErrorResponse{}
		errResp.Error = "about is not string"
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}

	name = data["first_name"].(string) + " " + data["last_name"].(string)
	about = data["about"].(string)

	}

	users, err := SearchXml(name,about,Ofset,Limit)

	if err != nil {
		//fmt.Println("unexpected error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w,"")
		return

		
	}

	// sort by order param
	
	err = Sort(users,OrderParam,OrderBy)

	if err != nil {
		errResp := SearchErrorResponse{}
		errResp.Error = ErrorBadOrderField
		w.WriteHeader(http.StatusBadRequest)
		
		result, _ := json.Marshal(errResp)
		io.WriteString(w,string(result))

		return
	}


	//Response user
	result, err := json.Marshal(users)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w,"")
		return
	}

	w.WriteHeader(http.StatusOK)
	io.WriteString(w,string(result))

}


// Bad Servers

func BadRequestServer(w http.ResponseWriter, r *http.Request) {
	/*errResp := SearchErrorResponse{}
	errResp.Error = ErrorBadOrderField*/
	w.WriteHeader(http.StatusBadRequest)
	
	
	//result, _ := json.Marshal(errResp)
	io.WriteString(w,"{")

	return
}

func BadResponseServer(w http.ResponseWriter, r *http.Request) {
	
	w.WriteHeader(http.StatusOK)
	io.WriteString(w,"{")

	return
}

