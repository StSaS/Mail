package main

import (
	"time"
	"testing"
	"net/http/httptest"
	"net/http"
)

func TestDummy(t *testing.T) {
	//t.Errorf("TODO")
}

func TestInternalServer(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer)) 
	sc := SearchClient{
		"d41d8cd98f00b204e9800998ecf8427e",
		ts.URL,
	}
	sr := SearchRequest {
		0,
		50, 
		`{"first_name":"","last_name":"","about":""}`,
		"Id",
		// -1 по убыванию, 0 как встретилось, 1 по возрастанию
		-1,
	}

	path = ""

	_, err := sc.FindUsers(sr)

	path = "dataset.xml"

	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	ts.Close()
}
func TestAcessToken(t *testing.T) {
	
	ts := httptest.NewServer(http.HandlerFunc(SearchServer)) 
	sc := SearchClient{
		"41d8cd98f00b204e9800998ecf8427e",
		ts.URL,
	}
	sr := SearchRequest {
		0,
		50, 
		`{"first_name":"","last_name":"","about":""}`,
		"Id",
		// -1 по убыванию, 0 как встретилось, 1 по возрастанию
		-1,
	}

	_, err := sc.FindUsers(sr)

	if err.Error() != "Bad AccessToken" {
		t.Errorf("unexpected error: %v", err)
	}

	ts.Close()
}

func TestUrl(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer)) 
	sc := SearchClient{
		"d41d8cd98f00b204e9800998ecf8427e",
		"example.com",
	}
	sr := SearchRequest {
		0,
		50, 
		`{"first_name":"","last_name":"","about":""}`,
		"Id",
		// -1 по убыванию, 0 как встретилось, 1 по возрастанию
		-1,
	}

	_, err := sc.FindUsers(sr)

	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	ts.Close()
}

func TestBadRequestServer(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(BadRequestServer)) 
	sc := SearchClient{
		"d41d8cd98f00b204e9800998ecf8427e",
		ts.URL,
	}
	sr := SearchRequest {
		0,
		0, 
		`{"first_name":"","last_name":"","about":""}`,
		"Id",
		// -1 по убыванию, 0 как встретилось, 1 по возрастанию
		-1,
	}

	_, err := sc.FindUsers(sr)
	
		if err != nil {
			t.Errorf("unexpected error: %v", err)
		}

	ts.Close()	

}

func TestBadResponseServer(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(BadResponseServer)) 
	sc := SearchClient{
		"d41d8cd98f00b204e9800998ecf8427e",
		ts.URL,
	}
	sr := SearchRequest {
		0,
		0, 
		`{"first_name":"","last_name":"","about":""}`,
		"Id",
		// -1 по убыванию, 0 как встретилось, 1 по возрастанию
		-1,
	}

	_, err := sc.FindUsers(sr)
	
		if err != nil {
			t.Errorf("unexpected error: %v", err)
		}

	ts.Close()	

}
func TestTimeout(t *testing.T) {
	
	ts := httptest.NewServer(http.HandlerFunc(SearchServer)) 
	sc := SearchClient{
		"d41d8cd98f00b204e9800998ecf8427e",
		ts.URL,
	}
	sr := SearchRequest {
		0,
		50, 
		`{"first_name":"","last_name":"","about":""}`,
		"Id",
		// -1 по убыванию, 0 как встретилось, 1 по возрастанию
		-1,
	}
	client  = &http.Client{Timeout: time.Microsecond}

	_, err := sc.FindUsers(sr)

	if err.Error() != "Bad AccessToken" {
		t.Errorf("unexpected error: %v", err)
	}

	client  = &http.Client{Timeout: time.Second}

	ts.Close()
}


func TestServer(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer)) 
	sc := SearchClient{
		"d41d8cd98f00b204e9800998ecf8427e",
		ts.URL,
	}

	cases := []SearchRequest{
		// next_page = false
		/*SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""}`,
			"Id",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},*/
		// next_page = true
		SearchRequest {
			0,
			20, 
			`{"first_name":"","last_name":"","about":""}`,
			"Id",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},

		// sort by Age
		/*SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""}`,
			"Age",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},

		// sort by Name
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""}`,
			"Name",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// sort by Name (empty)
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// sort with 1
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			1,
		},
		// sort with 0
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			0,
		},
		// find by about
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":"Lorem proident sint minim anim commodo cillum. Eiusmod velit culpa commodo anim consectetur consectetur sint sint labore. Mollit consequat consectetur magna nulla veniam commodo eu ut et. Ut adipisicing qui ex consectetur officia sint ut fugiat ex velit cupidatat fugiat nisi non. Dolor minim mollit aliquip veniam nostrud. Magna eu aliqua Lorem aliquip."}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// find by about and name
		SearchRequest {
			0,
			50, 
			`{"first_name":"Kane","last_name":"Sharp","about":"Lorem proident sint minim anim commodo cillum. Eiusmod velit culpa commodo anim consectetur consectetur sint sint labore. Mollit consequat consectetur magna nulla veniam commodo eu ut et. Ut adipisicing qui ex consectetur officia sint ut fugiat ex velit cupidatat fugiat nisi non. Dolor minim mollit aliquip veniam nostrud. Magna eu aliqua Lorem aliquip."}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// find by name
		SearchRequest {
			0,
			50, 
			`{"first_name":"Kane","last_name":"Sharp","about":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			0,
		},*/
	}

	for idx, item := range cases {
		
		_, err := sc.FindUsers(item)

		if err != nil {
			t.Errorf("[%d] expected error here %v", idx)
			continue
		}
	}

	// только неправильные считаются
	ts.Close()

}

func TestClient(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer)) 
	sc := SearchClient{
		"d41d8cd98f00b204e9800998ecf8427e",
		ts.URL,
	}

	cases := []SearchRequest{
		// normal
		SearchRequest {
			50,
			0, 
			`{"first_name":"","last_name":"","about":""}`,
			"Id",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// limit < 0
		/*SearchRequest {
			-10,
			0, 
			`{"first_name":"","last_name":"","about":""}`,
			"Id",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// offset < 0
		SearchRequest {
			0,
			-10, 
			`{"first_name":"","last_name":"","about":""}`,
			"Id",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},

		// search param error
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""`,
			"Age",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},

		// sort param error
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","about":""}`,
			"N",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// first_name is фиыуте
		SearchRequest {
			0,
			50, 
			`{"frst_name":"","last_name":"","about":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// last_name is absent
		SearchRequest {
			0,
			50, 
			`{"first_name":"","lat_name":"","about":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			1,
		},
		// about is absent
		SearchRequest {
			0,
			50, 
			`{"first_name":"","last_name":"","bout":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			0,
		},
		// find by about
		SearchRequest {
			50,
			10, 
			`{"first_name":"","last_name":"","about":"Lorem proident sint minim anim commodo cillum. Eiusmod velit culpa commodo anim consectetur consectetur sint sint labore. Mollit consequat consectetur magna nulla veniam commodo eu ut et. Ut adipisicing qui ex consectetur officia sint ut fugiat ex velit cupidatat fugiat nisi non. Dolor minim mollit aliquip veniam nostrud. Magna eu aliqua Lorem aliquip."}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		SearchRequest {
			50,
			10, 
			`[] {{"first_name":"","last_name":"","about":"Lorem proident sint minim anim commodo cillum. Eiusmod velit culpa commodo anim consectetur consectetur sint sint labore. Mollit consequat consectetur magna nulla veniam commodo eu ut et. Ut adipisicing qui ex consectetur officia sint ut fugiat ex velit cupidatat fugiat nisi non. Dolor minim mollit aliquip veniam nostrud. Magna eu aliqua Lorem aliquip."},{"first_name":"","last_name":"","bout":""}}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		/*SearchRequest {
			50,
			-10, 
			`{"first_name":"","last_name":"","about":"Lorem proident sint minim anim commodo cillum. Eiusmod velit culpa commodo anim consectetur consectetur sint sint labore. Mollit consequat consectetur magna nulla veniam commodo eu ut et. Ut adipisicing qui ex consectetur officia sint ut fugiat ex velit cupidatat fugiat nisi non. Dolor minim mollit aliquip veniam nostrud. Magna eu aliqua Lorem aliquip."}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// find by about and name
		SearchRequest {
			0,
			50, 
			`{"first_name":"Kane","last_name":"Sharp","about":"Lorem proident sint minim anim commodo cillum. Eiusmod velit culpa commodo anim consectetur consectetur sint sint labore. Mollit consequat consectetur magna nulla veniam commodo eu ut et. Ut adipisicing qui ex consectetur officia sint ut fugiat ex velit cupidatat fugiat nisi non. Dolor minim mollit aliquip veniam nostrud. Magna eu aliqua Lorem aliquip."}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			-1,
		},
		// find by name
		SearchRequest {
			0,
			50, 
			`{"first_name":"Kane","last_name":"Sharp","about":""}`,
			"",
			// -1 по убыванию, 0 как встретилось, 1 по возрастанию
			0,
		},*/
	}

	for idx, item := range cases {
		
		_, err := sc.FindUsers(item)

		if err != nil {
			t.Errorf("[%d] expected error here %v", idx)
			continue
		}
		if err == nil {
			t.Errorf("[%d] expected error here %v", idx)
			continue
		}
	}

	// только неправильные считаются
	ts.Close()

}
// сюда писать тесты
/*
ts := httptest.NewServer(http.HandlerFunc(SearchServer)) // перевести в client_test создать много тестов
fmt.Println(ts.URL)
sc := SearchClient{
	"d41d8cd98f00b204e9800998ecf8427e",
	ts.URL,
}
sr := SearchRequest {
	0,
	50, 
	`{"first_name":"","last_name":"","about":""}`,
	"Id",
	// -1 по убыванию, 0 как встретилось, 1 по возрастанию
	-1,
}
*/

/*
неправильный токен
неправильный url
разные варианты SearchRequest - правильные
разные варианты SearchRequest - неправильные
*/