package main

import (
	//"encoding/json"
	"errors"
	"fmt"
	"reflect"
)

func i2s(data interface{}, out interface{}) error {
	// todo
	switch reflect.TypeOf(data).Kind() {

	case reflect.Slice:
		s := reflect.ValueOf(data)
		vf := reflect.Indirect(reflect.ValueOf(out))

		tys := vf.Type()

		if tys.Kind() != reflect.Slice {
			return errors.New("No slice")
		}

		vys := reflect.MakeSlice(tys, s.Len(), s.Len())

		for i := 0; i < s.Len(); i++ {

			i2s(data.([]interface{})[i], vys.Index(i).Addr().Interface())

		}

		ptrval := reflect.ValueOf(out)

		sliceValue := reflect.Indirect(ptrval)

		sliceValue.Set(vys)

	case reflect.Map:
		dat := data.(map[string]interface{})

		if reflect.Indirect(reflect.ValueOf(out)) == reflect.ValueOf(out) {
			return errors.New("no reference")
		}
		val := reflect.ValueOf(out).Elem()

		for i := 0; i < val.NumField(); i++ {
			typeField := val.Type().Field(i)

			switch typeField.Type.Kind() {
			case reflect.Int:
				if reflect.TypeOf(dat[typeField.Name]).Kind() != reflect.Float64 {
					return errors.New("no match Float64")
				}
				val.Field(i).Set(reflect.ValueOf(int(dat[typeField.Name].(float64))))
			case reflect.Bool:
				if reflect.TypeOf(dat[typeField.Name]).Kind() != reflect.Bool {
					return errors.New("no match Bool")
				}
				val.Field(i).Set(reflect.ValueOf(bool(dat[typeField.Name].(bool))))
			case reflect.String:

				if reflect.TypeOf(dat[typeField.Name]).Kind() != reflect.String {
					return errors.New("no match String")
				}
				val.Field(i).SetString(dat[typeField.Name].(string))
			case reflect.Struct:

				if reflect.TypeOf(dat[typeField.Name]).Kind() != reflect.Map {
					return errors.New("no match Struct")
				}
				i2s(dat[typeField.Name].(map[string]interface{}), val.Field(i).Addr().Interface())
			case reflect.Slice:

				if reflect.TypeOf(dat[typeField.Name]).Kind() != reflect.Slice {
					return errors.New("no match Slice")
				}
				i2s(dat[typeField.Name].([]interface{}), val.Field(i).Addr().Interface())

			default:
				return fmt.Errorf("bad type: %v for field %v", typeField.Type.Kind(), typeField.Name)
			}

		}

	}

	return nil
}

func main() {

}
