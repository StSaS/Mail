package main

import (
	//"go/format"
	"bytes"
	"fmt"
	"go/ast"
	"go/printer"
	"io"
	"strconv"
	"text/template"
	//"go/ast"
	"encoding/json"
	"go/parser"
	"go/token"
	"log"
	"os"
	"reflect"
	"strings"
	//"text/template"
)

type tpl struct {
	Field string
	Acc   string
}

type tplParams struct {
	Name    string
	Default string
	Field   string
	Value   string
	Acc     string
}

type tplCase struct {
	Url  string
	Func string
}

// это кодогенератор
var (
	caseFunc = template.Must(template.New("caseFuncTpl").Parse(`
	case "{{.Url}}" :
		err, res := h.{{.Func}}(w,r)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			io.WriteString(w,res)
		} else {
			m := make(map[string]string)
			m["error"] = err.Error()
			b,_ := json.Marshal(m)
			io.WriteString(w,string(b))
		}
		`))

	checkInt = template.Must(template.New("intTpl").Parse(`
	{{.Name}} , err := strconv.Atoi({{.Field}})
	if (err != nil) && ({{.Field}} != "") {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("{{.Field}} must be int"),""
	}`))

	checkMinInt = template.Must(template.New("minIntTpl").Parse(`if {{.Acc}}.{{.Field}} < {{.Default}} {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("{{.Name}} must be >= {{.Default}}"),""
	}`))

	checkMinStr = template.Must(template.New("minStrTpl").Parse(`if len({{.Acc}}.{{.Field}}) < {{.Default}} {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("{{.Name}} len must be >= {{.Default}}"),""
	}`))

	checkMaxInt = template.Must(template.New("maxIntTpl").Parse(`if {{.Acc}}.{{.Field}} > {{.Default}} {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("{{.Name}} must be <= {{.Default}}"),""
	}`))

	checkMaxStr = template.Must(template.New("maxStrTpl").Parse(`if len({{.Acc}}.{{.Field}}) > {{.Default}} {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("{{.Name}} len must be <= {{.Default}}"),""
	}`))

	structTpl = template.Must(template.New("structTpl").Parse(`var {{.Acc}} {{.Field}} = {{.Field}}{`))

	checkRequired = template.Must(template.New("reqTpl").Parse(`	if {{.Acc}}.{{.Name}} == {{.Default}}  {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("{{.Field}} must me not empty"), ""
		} `))

	checkDefault = template.Must(template.New("defTpl").Parse(`	if {{.Acc}}.{{.Field}} == "{{.Default}}"  {
			{{.Acc}}.{{.Field}} = "{{.Value}}"
		} `))

	methodPostValid = `if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotAcceptable)
		return fmt.Errorf("bad method"),""
	}
	`

	authValid = `if r.Header.Get("X-Auth") != "100500" {
		w.WriteHeader(http.StatusForbidden)
		return fmt.Errorf("unauthorized"),""
	}
	`

	listImpopt = `
	import (
		"fmt"
		"net/http"
		"encoding/json"
		"strconv"
		"io"
	)
	`

	marshalResult = template.Must(template.New("marshResTpl").Parse(`
	acc := map[string]interface{} {
		"error": "",
		"response": {{.Name}},
	}`))
)

func GetStruct(out io.Writer, node *ast.File, fset *token.FileSet, nameStruct string, varName string) {

	if nameStruct == "context.Context" {
		fmt.Fprintln(out, varName+" := r.Context()")
		return
	}

	GetParametrs(out, node, fset, nameStruct)

	for _, decl := range node.Decls {

		g, ok := decl.(*ast.GenDecl)

		if !ok {
			continue
		}

		for _, spec := range g.Specs {
			currType, ok := spec.(*ast.TypeSpec)
			if !ok {

				continue
			}

			currStruct, ok := currType.Type.(*ast.StructType)
			if !ok {

				continue
			}

			var buf bytes.Buffer

			printer.Fprint(&buf, fset, currType.Name)
			//fmt.Fprintln(out,buf.String())
			//fmt.Println(buf.String())
			if nameStruct == buf.String() {

				for _, field := range currStruct.Fields.List {

					var buf bytes.Buffer
					printer.Fprint(&buf, fset, field.Type)

					if buf.String() == "int" {
						params := tplParams{
							Name:  CheckParam(field) + "Int",
							Field: CheckParam(field),
							Acc:   varName,
						}
						checkInt.Execute(out, params)
						fmt.Fprintln(out, "")
					}
				}

				structTpl.Execute(out, tpl{nameStruct, varName})
				fmt.Fprintln(out, "")

				for _, field := range currStruct.Fields.List {

					var buf bytes.Buffer
					printer.Fprint(&buf, fset, field.Type)

					switch buf.String() {
					case "string":
						fmt.Fprint(out, field.Names[0].Name+":")

						fmt.Fprintln(out, CheckParam(field)+",") // cделать нижний регистр и проверку на paramname

					case "int":
						fmt.Fprint(out, field.Names[0].Name+":")

						fmt.Fprintln(out, CheckParam(field)+"Int,")

					}
				}
				fmt.Fprintln(out, " }")
				Validator(out, currStruct, fset, varName)
			}
		}

	}

}

func CheckParam(field *ast.Field) string {

	if field.Tag != nil {
		tag := reflect.StructTag(field.Tag.Value[1 : len(field.Tag.Value)-1])
		if strings.HasPrefix(string(tag), "apivalidator") {

			validator := tag.Get("apivalidator")

			tags := strings.Split(validator, ",")

			//если int конвертировать

			for _, t := range tags {

				if strings.Contains(t, "paramname=") {
					param_name := strings.TrimPrefix(t, "paramname=")
					//enums := strings.Split(enum,"|")
					return param_name
					//fmt.Fprintln(out,param_name)
				}

			}

		}
	}

	return strings.ToLower(field.Names[0].Name)
}

func Validator(out io.Writer, currStruct *ast.StructType, fset *token.FileSet, varName string) {

	for _, field := range currStruct.Fields.List {

		if field.Tag != nil {
			tag := reflect.StructTag(field.Tag.Value[1 : len(field.Tag.Value)-1])
			if strings.HasPrefix(string(tag), "apivalidator") {

				validator := tag.Get("apivalidator")

				tags := strings.Split(validator, ",")

				//fmt.Fprintln(out,tags)

				for _, t := range tags {

					if strings.Contains(t, "required") {
						//fmt.Fprintln(out,"required")

						var buf bytes.Buffer
						printer.Fprint(&buf, fset, field.Type)

						switch buf.String() {
						case "int":

							params := tplParams{
								Name:    field.Names[0].Name,
								Default: "-1",
								Field:   CheckParam(field),
								Acc:     varName,
							}
							checkRequired.Execute(out, params)

						case "string":

							params := tplParams{
								Name:    field.Names[0].Name,
								Default: "\"\"",
								Field:   CheckParam(field),
								Acc:     varName,
							}
							checkRequired.Execute(out, params)
							fmt.Fprintln(out, " ")

						}

					}

					if strings.Contains(t, "max=") {
						var buf bytes.Buffer
						printer.Fprint(&buf, fset, field.Type)

						switch buf.String() {
						case "int":
							params := tplParams{
								Name:    CheckParam(field),
								Default: strings.TrimPrefix(t, "max="),
								Field:   field.Names[0].Name,
								Acc:     varName,
							}
							checkMaxInt.Execute(out, params)
							fmt.Fprintln(out)

						case "string":

							params := tplParams{
								Name:    CheckParam(field),
								Default: strings.TrimPrefix(t, "max="),
								Field:   field.Names[0].Name,
								Acc:     varName,
							}
							checkMaxStr.Execute(out, params)
							fmt.Fprintln(out)
							//длина строки
						}
					}

					if strings.Contains(t, "min=") {

						var buf bytes.Buffer
						printer.Fprint(&buf, fset, field.Type)

						switch buf.String() {
						case "int":
							params := tplParams{
								Name:    CheckParam(field),
								Default: strings.TrimPrefix(t, "min="),
								Field:   field.Names[0].Name,
								Acc:     varName,
							}
							checkMinInt.Execute(out, params)
							fmt.Fprintln(out)

						case "string":

							params := tplParams{
								Name:    CheckParam(field),
								Default: strings.TrimPrefix(t, "min="),
								Field:   field.Names[0].Name,
								Acc:     varName,
							}
							checkMinStr.Execute(out, params)
							fmt.Fprintln(out)

						}

					}

					if strings.Contains(t, "enum=") {
						enum := strings.TrimPrefix(t, "enum=")
						enums := strings.Split(enum, "|")

						var buf bytes.Buffer
						printer.Fprint(&buf, fset, field.Type)

						switch buf.String() {
						case "string":

							for i, e := range enums {
								if i != 0 {
									fmt.Fprint(out, " && ")
								} else {
									fmt.Fprint(out, " if ")
								}
								fmt.Fprint(out, "!( "+varName+"."+field.Names[0].Name+" == "+"\""+e+"\""+" )")
							}

							//empty
							fmt.Fprint(out, " && ")
							fmt.Fprint(out, "!( "+varName+"."+field.Names[0].Name+" == \"\" )")

							fmt.Fprintln(out, " {")
							//сгенерировать из enums строку
							fmt.Fprintln(out, "w.WriteHeader(http.StatusBadRequest)")
							fmt.Fprint(out, "return fmt.Errorf( \""+CheckParam(field)+" must be one of ")

							fmt.Fprint(out, "[")

							for i, e := range enums {
								fmt.Fprint(out, e)
								if i != len(enums)-1 {
									fmt.Fprint(out, ", ")
								} else {
									fmt.Fprint(out, "]")
								}
							}

							fmt.Fprint(out, "\"),\"\"")
							//fmt.Println(enums)
							fmt.Fprintln(out)
							fmt.Fprintln(out, " }")
							// (!(acc.status == err) && !(acc.status == ok)||())
						}
					}

					if strings.Contains(t, "default=") {
						param_name := strings.TrimPrefix(t, "default=")

						var buf bytes.Buffer
						printer.Fprint(&buf, fset, field.Type)

						switch buf.String() {
						case "string":

							params := tplParams{
								Value:   param_name,
								Default: "",
								Field:   field.Names[0].Name,
								Acc:     varName,
							}

							checkDefault.Execute(out, params)
							fmt.Fprintln(out)
						}
						//enums := strings.Split(enum,"|")
						//fmt.Fprintln(out,param_name)

					}
				}

			}
		}
	}

}

func GetParametrs(out io.Writer, node *ast.File, fset *token.FileSet, nameStruct string) {

	for _, decl := range node.Decls {

		g, ok := decl.(*ast.GenDecl)

		if !ok {
			continue
		}

		for _, spec := range g.Specs {
			currType, ok := spec.(*ast.TypeSpec)
			if !ok {

				continue
			}

			currStruct, ok := currType.Type.(*ast.StructType)
			if !ok {

				continue
			}

			var buf bytes.Buffer

			printer.Fprint(&buf, fset, currType.Name)
			//fmt.Fprintln(out,buf.String())
			//fmt.Println(buf.String())
			if nameStruct == buf.String() {

				for _, field := range currStruct.Fields.List {

					fmt.Fprintln(out, "var "+CheckParam(field)+" string")

				}

				fmt.Fprintln(out, "if r.Method == http.MethodPost {")

				// POST
				for _, field := range currStruct.Fields.List {

					fmt.Fprintln(out, CheckParam(field)+" = r.FormValue(\""+CheckParam(field)+`")`)

				}

				fmt.Fprintln(out, "} else {")

				// GET
				for _, field := range currStruct.Fields.List {

					fmt.Fprintln(out, CheckParam(field)+" = r.URL.Query().Get(\""+CheckParam(field)+`")`)

				}

				fmt.Fprintln(out, "}")

				for _, field := range currStruct.Fields.List {

					fmt.Fprintln(out, CheckParam(field)+" = "+CheckParam(field))

				}
			}
		}
	}
}

func main() {

	// header
	urls := make(map[string]string)
	var methodOfStruct string

	fset := token.NewFileSet()
	//node, err := parser.ParseFile(fset, os.Args[1], nil, parser.ParseComments)
	node, err := parser.ParseFile(fset, "..//site//api.go", nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	//out, _ := os.Create(os.Args[2])
	out, _ := os.Create("..//site//http.go")

	fmt.Fprintln(out, `package `+node.Name.Name)
	fmt.Fprintln(out, listImpopt)
	fmt.Fprintln(out) // empty line

	for _, decl := range node.Decls {

		switch decl.(type) {

		case *ast.FuncDecl:
			f, _ := decl.(*ast.FuncDecl)

			//fmt.Fprintln(out,f.Body)
			if f.Doc == nil {
				continue
			}

			for _, comm := range f.Doc.List {
				if strings.Contains(comm.Text, "apigen:api") {

					//var methodOfStruct string

					if f.Recv != nil {
						a := f.Recv.Opening - comm.Slash
						b := f.Recv.Closing - comm.Slash

						var buf bytes.Buffer
						printer.Fprint(&buf, fset, f)
						//fmt.Fprintln(out,buf.String()[a:b]) // Type of in parmetr
						methodOfStruct = " " + buf.String()[a:b] + ") "
					}

					fmt.Fprintln(out, "func"+methodOfStruct+" Handler"+f.Name.Name+"(w http.ResponseWriter,r *http.Request) (error,string) {")

					jsonstr := strings.TrimPrefix(comm.Text, "// apigen:api ")

					var lowers map[string]interface{}
					//var str string

					json.Unmarshal([]byte(jsonstr), &lowers)
					//fmt.Fprintln(out,strings.TrimPrefix(comm.Text,"// apigen:api "))
					//fmt.Fprintln(out,int(comm.Slash) + len(jsonstr))

					if val, ok := lowers["url"]; ok {
						fmt.Fprintln(out, "// "+val.(string))
						urls[val.(string)] = "Handler" + f.Name.Name
					}

					if val, ok := lowers["method"]; ok {
						//fmt.Fprintln(out, val.(string))
						if val.(string) == "POST" {
							fmt.Fprintln(out, methodPostValid)
							//fmt.Fprintln(out, getQuery)
							//fmt.Fprintln(out,getQueryPost)
						}
					} else {
						//fmt.Fprintln(out,getQueryGet)
						//fmt.Fprintln(out, getQuery)
					}

					if val, ok := lowers["auth"]; ok {
						//fmt.Fprintln(out, val.(bool))
						if val.(bool) == true {
							fmt.Fprintln(out, authValid)
						}
					}

					var in string
					in = "("

					for i, param := range f.Type.Params.List {
						//fmt.Fprintln(out,strconv.Itoa(i) + param.Names[0].Name)

						in += param.Names[0].Name
						if i != len(f.Type.Params.List)-1 {
							in += ","
						} else {
							in += ")"
						}

						var buf bytes.Buffer
						printer.Fprint(&buf, fset, param.Type)
						//fmt.Fprintln(out,buf.String()) // Type of in parmetr

						GetStruct(out, node, fset, buf.String(), param.Names[0].Name)

					}

					var outMethod string
					outMethod = ""

					var result string
					var errorResult string

					for i, param := range f.Type.Results.List {

						var buf bytes.Buffer
						printer.Fprint(&buf, fset, param.Type)

						if buf.String() == "error" {

							outMethod += "err" + f.Name.Name

							errorResult = "err" + f.Name.Name
						} else {
							outMethod += "res" + f.Name.Name + strconv.Itoa(i)
							result = "res" + f.Name.Name + strconv.Itoa(i)
						}

						if i != len(f.Type.Results.List)-1 {
							outMethod += ","
						} else {
							outMethod += " := "
						}
					}

					//fmt.Fprintln(out,f.Name.Obj.)
					fmt.Fprint(out, outMethod)
					//fmt.Fprint(out,methodOfStruct)
					fmt.Fprint(out, strings.Trim(strings.Split(methodOfStruct, " ")[1], "(")+".")
					fmt.Fprint(out, f.Name.Name) // добавить methodOfStruct (srv.))
					fmt.Fprintln(out, in)

					fmt.Fprintln(out, " if "+errorResult+" != nil {")
					fmt.Fprintln(out, " if "+errorResult+".Error() == \"bad user\"  {")
					fmt.Fprintln(out, `w.WriteHeader(http.StatusInternalServerError)
							} else {`)
					fmt.Fprintln(out, `w.WriteHeader(`+errorResult+`.(ApiError).HTTPStatus)
							}`)

					fmt.Fprintln(out, ` return `+errorResult+`,""`)
					fmt.Fprintln(out, "}")

					params := tplParams{
						Name: result,
					}
					marshalResult.Execute(out, params)
					fmt.Fprintln(out)

					fmt.Fprintln(out, "res, _ := json.Marshal(acc)")
					fmt.Fprintln(out, " return nil,string(res)")

				}

			}

			fmt.Fprintln(out, "}")
		}

	}

	//var myapi string
	a := strings.Index(methodOfStruct,"*")
	b := strings.Index(methodOfStruct,")")

	fmt.Fprintln(out, `func (h ` + methodOfStruct[a:b+1] + ` ServeHTTP(w http.ResponseWriter, r *http.Request) {`)
	fmt.Fprintln(out, `	switch r.URL.Path {`)

	for v := range urls {
		params := tplCase{
			Url:  v,
			Func: urls[v],
		}

		caseFunc.Execute(out, params)
		fmt.Fprintln(out)
	}
	fmt.Fprintln(out, `default:
			w.WriteHeader(404)
			m := make(map[string]string)
			m["error"] = "unknown method"
			b,_ := json.Marshal(m)
			io.WriteString(w,string(b))
	
		}
	}	`)
	//**********************************************

}
