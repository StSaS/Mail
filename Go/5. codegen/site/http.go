package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
)

func (srv *MyApi) HandlerProfile(w http.ResponseWriter, r *http.Request) (error, string) {
	// /user/profile
	ctx := r.Context()
	var login string
	if r.Method == http.MethodPost {
		login = r.FormValue("login")
	} else {
		login = r.URL.Query().Get("login")
	}
	login = login
	var in ProfileParams = ProfileParams{
		Login: login,
	}
	if in.Login == "" {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("login must me not empty"), ""
	}
	resProfile0, errProfile := srv.Profile(ctx, in)
	if errProfile != nil {
		if errProfile.Error() == "bad user" {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			w.WriteHeader(errProfile.(ApiError).HTTPStatus)
		}
		return errProfile, ""
	}

	acc := map[string]interface{}{
		"error":    "",
		"response": resProfile0,
	}
	res, _ := json.Marshal(acc)
	return nil, string(res)
}
func (srv *MyApi) HandlerCreate(w http.ResponseWriter, r *http.Request) (error, string) {
	// /user/create
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotAcceptable)
		return fmt.Errorf("bad method"), ""
	}

	if r.Header.Get("X-Auth") != "100500" {
		w.WriteHeader(http.StatusForbidden)
		return fmt.Errorf("unauthorized"), ""
	}

	ctx := r.Context()
	var login string
	var full_name string
	var status string
	var age string
	if r.Method == http.MethodPost {
		login = r.FormValue("login")
		full_name = r.FormValue("full_name")
		status = r.FormValue("status")
		age = r.FormValue("age")
	} else {
		login = r.URL.Query().Get("login")
		full_name = r.URL.Query().Get("full_name")
		status = r.URL.Query().Get("status")
		age = r.URL.Query().Get("age")
	}
	login = login
	full_name = full_name
	status = status
	age = age

	ageInt, err := strconv.Atoi(age)
	if (err != nil) && (age != "") {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("age must be int"), ""
	}
	var in CreateParams = CreateParams{
		Login:  login,
		Name:   full_name,
		Status: status,
		Age:    ageInt,
	}
	if in.Login == "" {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("login must me not empty"), ""
	}
	if len(in.Login) < 10 {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("login len must be >= 10"), ""
	}
	if !(in.Status == "user") && !(in.Status == "moderator") && !(in.Status == "admin") && !(in.Status == "") {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("status must be one of [user, moderator, admin]"), ""
	}
	if in.Status == "" {
		in.Status = "user"
	}
	if in.Age < 0 {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("age must be >= 0"), ""
	}
	if in.Age > 128 {
		w.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("age must be <= 128"), ""
	}
	resCreate0, errCreate := srv.Create(ctx, in)
	if errCreate != nil {
		if errCreate.Error() == "bad user" {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			w.WriteHeader(errCreate.(ApiError).HTTPStatus)
		}
		return errCreate, ""
	}

	acc := map[string]interface{}{
		"error":    "",
		"response": resCreate0,
	}
	res, _ := json.Marshal(acc)
	return nil, string(res)
}
func (h *MyApi) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {

	case "/user/profile":
		err, res := h.HandlerProfile(w, r)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			io.WriteString(w, res)
		} else {
			m := make(map[string]string)
			m["error"] = err.Error()
			b, _ := json.Marshal(m)
			io.WriteString(w, string(b))
		}

	case "/user/create":
		err, res := h.HandlerCreate(w, r)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			io.WriteString(w, res)
		} else {
			m := make(map[string]string)
			m["error"] = err.Error()
			b, _ := json.Marshal(m)
			io.WriteString(w, string(b))
		}

	default:
		w.WriteHeader(404)
		m := make(map[string]string)
		m["error"] = "unknown method"
		b, _ := json.Marshal(m)
		io.WriteString(w, string(b))

	}
}
