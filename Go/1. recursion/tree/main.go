package main

import (
	"bytes"
	"io"
	"io/ioutil"
	//"path/filepath"
	"strconv"
	//"fmt"
	//"io"
	//"bytes"
	"os"
	//"path/filepath"
	//"strings"
)

func MakeTreeFull(out io.Writer, path string, flag bool, prefix string) (err error) {

	list, err := ioutil.ReadDir(path)

	if err != nil {
		return err
	}

	lastdir := ""
	for idx := range list {
		if list[idx].IsDir() == true {
			lastdir = list[idx].Name()
		}
	}

	for idx := range list {

		if list[idx].IsDir() == true {
			dir := list[idx].Name()

			if ((list[idx].Name() == lastdir) && (flag == false)) || ((idx == len(list)-1) && (flag == true)) {

				out.Write([]byte(prefix))
				out.Write([]byte("└───" + dir + "\n"))

				MakeTreeFull(out, path+"//"+dir, flag, prefix+"	")

			} else {

				out.Write([]byte(prefix))
				out.Write([]byte("├───" + dir + "\n"))
				MakeTreeFull(out, path+"//"+dir, flag, prefix+"│	")

			}
		} else if flag == true {
			file := list[idx].Name()

			if idx == len(list)-1 {

				out.Write([]byte(prefix))
				size := list[idx].Size()

				var sizestr string

				if size == 0 {
					sizestr = " (empty)"
				} else {
					sizestr = " (" + strconv.Itoa(int(size)) + "b)"
				}

				out.Write([]byte("└───" + file + sizestr + "\n"))

			} else {

				out.Write([]byte(prefix))

				size := list[idx].Size()

				var sizestr string

				if size == 0 {
					sizestr = " (empty)"

				} else {
					if list[idx].Name() == "main.go" {
						sizestr = " (vary)"
					} else {
						sizestr = " (" + strconv.Itoa(int(size)) + "b)"
					}
				}

				out.Write([]byte("├───" + file + sizestr + "\n"))

			}
		}
	}

	return nil
}

func dirTree(out io.Writer, path string, flag bool) (err error) {

	err = MakeTreeFull(out, path, flag, "")
	if err != nil {
		return err
	}

	return nil
}

func main() {
	out := os.Stdout
	var buf bytes.Buffer
	w := io.MultiWriter(out, &buf)
	if !(len(os.Args) == 2 || len(os.Args) == 3) {

		panic("usage go run main.go . [-f]")

	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(w, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
