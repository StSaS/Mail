// тут лежит тестовый код
// менять вам может потребоваться только коннект к базе
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
)

var (
	// DSN это соединение с базой
	// вы можете изменить этот параметр, в тестах соединение будет браться отсюда
	DSN = "root:1234@tcp(localhost:3306)/golang-2017?charset=utf8&interpolateParams=true"
	//DSN = "root@tcp(localhost:3360)/golang2017?charset=utf8&interpolateParams=true"
)

func main() {
	db, err := sql.Open("mysql", DSN)
	err = db.Ping() // вот тут будет первое подключение к базе
	if err != nil {
		panic(err)
	}

	handler, err := NewDbCRUD(db)
	if err != nil {
		panic(err)
	}

	fmt.Println("starting server at :8082")
	http.ListenAndServe(":8082", handler)
}
