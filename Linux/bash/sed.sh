#!/bin/bash



space='s/  */ /g'

minus='s/---*/--/g'

toDOS='s/$/\r/;s/\r\r$/\r/;s/\n/\r\n/'

toUNIX='s/\x0D$//'

antilogy=0



rgx=""



if [ "$*" = "-h" ]

then

        echo "This program editing text stream"

        echo "-s | --space - delete spaces "

        echo "-m | --minus - delete minus"

        echo "-u | --dos2unix - convert from dos to unix format"

        echo "-d | --unix2dos - convert from unix to dos format"

        echo "Alex Stiharnyy && Nikita Ytkin"

        echo "	           PS-445"

        exit

fi





while getopts "dusm-:" opt; do



case $opt in





d)  if [ $antilogy -eq 0 ] 

    then 

	rgx="$rgx;$toDOS" 

        antilogy=1 

    else 

	echo "antilogy operations: 2dos and 2unix" >&2

        exit

    fi	 

    ;;



u)  if [ $antilogy -eq 0 ] 

    then

        rgx="$rgx;$toUNIX" 

        antilogy=1 

    else 

        echo "antilogy operations: 2dos and 2unix" >&2

        exit 

    fi

    ;;  



s)  rgx="$rgx;$space" ;;



m)  rgx="$rgx;$minus" ;;



-)  case "$OPTARG" in

                unix2dos) if [ $antilogy -eq 0 ] 

                          then

        			rgx="$rgx;$toDOS"

                                antilogy=1

    			  else

        			echo "antilogy operations: 2dos and 2unix" >&2

                                exit

    			  fi

    			  ;;

		dos2unix)   if [ $antilogy -eq 0 ] 

                            then

                                rgx="$rgx;$toUNIX"

                                antilogy=1

                            else 

                                echo "antilogy operations: 2dos and 2unix" >&2

                                exit

                            fi

                            ;;  

                spaces)  rgx="$rgx;$space" ;;

		minus) rgx="$rgx;$minus" ;;

		*) echo "No reasonable options found!" >&2 ; exit ;;

            esac;;



*) echo "No reasonable options found!" >&2 ; exit ;;





esac



done



ssed "$rgx"