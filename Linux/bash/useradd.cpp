#define _GNU_SOURCE
#define _XOPEN_SOURCE

#include <crypt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <cstdint>
#include <shadow.h>
//#include <stddef.h>

struct passwd passw;
struct group group;
char** memory;
struct stat st;
char login[255];
char fullname[255];
char password[255];
char line[255];


int getPaswID()
{
    // ������ � ��������
    struct passwd* password;
    u_int32_t x;
    bool f = true;

    while (f)
    {
        setpwent();
        password = getpwent();

        x = rand();
        f = false;

        while ( password != nullptr )
        {
            if (password->pw_uid == x)
                f = true;
            password = getpwent();
        }
    }


    return x;
}

int getGroupID()
{

    struct group* groups;
    u_int32_t x;
    bool f = true;



    while (f)
    {
        setgrent();
        groups = getgrent();

        x = rand();
        f = false;

        while ( groups != nullptr )
        {
            if (groups->gr_gid == x)
                f = true;
            groups = getgrent();
        }
    }


    return x;

}
//������������ ������ �� �������
char* getPassw() // �������� ����� ������
{
    char* pasw; // 10 symbols password default
    pasw = (char*)malloc(10);


    //int ret = system("./passwGen.sh -n 1");// ����������� �����
    FILE *fp = popen("./passwGen.sh -n 1", "r");


    fscanf(fp, "%s", pasw);



    //printf("%s",pasw);

    //free(pasw);
    pclose(fp);


     // popen use
    return pasw;
}

int getParametrs(char* line, char* login, char* fullname) //���������� ������
{
    char* tok = strtok(line, ";");
    if(tok == nullptr)
        return 0;
    strcpy(login, tok);

    tok = strtok(nullptr, ";\n");
    if(tok == nullptr )
        return 0;
    strcpy(fullname, tok);

    return 1;
}

char* crypt_passwd(const char* passwd)
{
    const char* seedchars =
        "./0123456789ABCDEFGHIJKLMNOPQRST"
        "UVWXYZabcdefghijklmnopqrstuvwxyz";
    const int seednum = strlen(seedchars);
    char salt[12];

    salt[0] = '$'; salt[1] = '1'; salt[2] = '$';  // md5
    for(int i = 0; i < 8; i++)
        salt[3 + i] = seedchars[rand() % seednum];

    return crypt(passwd, salt); // ��������� �� ������������
}

void add_to_shadow(char* user, char* passwd)
{
    if(getspnam(user) != nullptr)
    {
        printf("user %s already exists\n", user);
        return;
    }

    struct spwd p;
    //FILE* f = fopen("/home/alex/shadow", "a"); // /etc/shadow
    FILE* f = fopen("/etc/shadow", "a"); // /etc/shadow

    memset(&p, 0, sizeof(p));

    p.sp_namp = user;
    p.sp_pwdp = passwd;
    p.sp_lstchg = time(NULL) / (60*60*24);
    p.sp_min = -1;
    p.sp_max = 9001;
    p.sp_warn = -1;
    p.sp_inact = -1;
    p.sp_expire = -1;

    if(putspent(&p, f))
        perror("Error: ");

    fclose(f);
}

int interactive( char* line) // use getpwnam()
{

    /*struct passwd {
        char    *pw_name;         username
        char    *pw_passwd;       user password
        uid_t    pw_uid;         /* user ID
        gid_t    pw_gid;         /* group ID
        char    *pw_gecos;       /* real name
        char    *pw_dir;         /* home directory
        char    *pw_shell;       /* shell program
    */




   /* The struct group is defined as follows:

               struct group {
                   char   *gr_name;      /* group name
                   char   *gr_passwd;    /* group password
                   gid_t   gr_gid;       /* group ID
                   char  **gr_mem;       /* group members
               };

    */
    int user;
    user = getuid();

    if ( user != 0)
    {
        printf("not root\n");
        return 1;
    }

    int check = getParametrs(line, login, fullname);

    if (check == 0)
    {
        printf("wrong format\n");
        return 1;
    }

    if(getpwnam(fullname))
       {
           printf("user %s already exsists\n", fullname);
           return 1;
       }

    /*struct passwd {
        char    *pw_name;         username
        char    *pw_passwd;       user password
        uid_t    pw_uid;         /* user ID
        gid_t    pw_gid;         /* group ID
        char    *pw_gecos;       /* real name
        char    *pw_dir;         /* home directory
        char    *pw_shell;       /* shell program
    */




   /* The struct group is defined as follows:

               struct group {
                   char   *gr_name;      /* group name
                   char   *gr_passwd;    /* group password
                   gid_t   gr_gid;       /* group ID
                   char  **gr_mem;       /* group members
               };

    */

    char home[255] = "/home/";
    strcat(home,fullname);

    memset(&passw, 0, sizeof(passw));

    passw.pw_name = fullname;

    char* pw = getPassw();
    //passw.pw_passwd = pw; // ������� x � ��������� shadow
    passw.pw_passwd = "x";

    passw.pw_uid = getPaswID();
    passw.pw_gid = getGroupID(); //
    passw.pw_gecos = login;
    passw.pw_dir = home;
    passw.pw_shell = "/bin/bash";

    printf("login:%s password:%s uid:%d gid:%d homedir:%s\n",login, pw, passw.pw_uid, passw.pw_gid, home);

    add_to_shadow(fullname,crypt_passwd(pw));



    FILE * pFile;
    FILE * gFile;

    pFile = fopen ("/etc/passwd", "a"); // /etc/passwd
    //pFile = fopen ("/home/alex/passwd", "a"); // /etc/passwd


    int k = putpwent(&passw, pFile); // ������ � passwd ������������

    if ( k == -1)
    {
        printf("no superuser\n");
        return 1;
    }


    fclose(pFile);
    free(pw);


    // group

    memset(&group, 0, sizeof(group));

    group.gr_name = login;
    group.gr_passwd = "*";
    group.gr_gid = passw.pw_gid;


    char* plogin[2];
    plogin[0] = login;
    plogin[1] = 0;


    group.gr_mem =  plogin;

    gFile = fopen ("/etc/group", "a"); // /etc/group
    //gFile = fopen ("/home/alex/group", "a"); // /etc/group

    int m = putgrent(&group, gFile);

    if ( m == -1)
    {
        printf("no superuser\n");
        return 1;
    }

    fclose(gFile);

    int status;
    if (status = stat(home, &st) == -1) // � ������, ���� ��� ������� ����������
    {
         mkdir(home, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
         chown(home, passw.pw_uid, passw.pw_gid );

         if (status = stat(home, &st) == -1)
             printf("make directory error (do from root)\n");

    }




    return 0;
}



int main(int argc, char *argv[])
{


  /* struct passwd passw;
   struct group group;
   char** memory;
   struct stat st;
   char login[255];
   char fullname[255];
   char password[255];
   char line[255];*/



   if (argc == 1)
   {

       printf("enter <login>;<fullname>\n");
       scanf("%[^\n]", line);

       if (!strcmp(line, "exit"))
                    return 0;



       if (interactive(line) == 1)
           return 0;
   }
   else
   {
       if (!strcmp(argv[1], "-f"))
       {
           if(argc < 3)
                   {


                      printf("filepath doesnt enter\n");
                       return 0;
                   }


           int user;
           user = getuid();

           if ( user != 0)
           {
               printf("not root\n");
               return 0;
           }

            FILE* f = fopen(argv[2], "r");


            while(fgets(line, 255, f) != nullptr)
            {

                int check = getParametrs(line, login, fullname);

                if (check == 0)
                {
                    printf("wrong format\n");
                    continue;
                    //return 0;
                }

                if(getpwnam(fullname))
                {
                       printf("user already exsists\n");
                       printf("user %s already exsists\n", fullname);
                       continue;
                }

                char home[255] = "/home/";
                strcat(home,fullname);

                memset(&passw, 0, sizeof(passw));

                passw.pw_name = fullname;

                char* pw = getPassw();
                //passw.pw_passwd = pw; // �������� ������ �
                passw.pw_passwd = "x";

                passw.pw_uid = getPaswID(); // ��������� � ������ ���������� ���������
                passw.pw_gid = getGroupID(); //
                passw.pw_gecos = login;
                passw.pw_dir = home;
                passw.pw_shell = "/bin/bash";

                add_to_shadow(fullname,crypt_passwd(pw));

                printf("login:%s password:%s uid:%d gid:%d homedir:%s\n",login, pw, passw.pw_uid, passw.pw_gid, home);
                //printf("%s login:%s password:%s uid:%d gid:%d homedir:%s\n",passw.pw_name, passw.pw_gecos, passw.pw_uid, passw.pw_gid, passw.pw_dir);

                FILE * pFile;
                FILE * gFile;

                pFile = fopen ("/etc/passwd", "a");
                //pFile = fopen ("/home/alex/passwd", "a");


                int k = putpwent(&passw, pFile); // ������ � passwd ������������

                if ( k == -1)
                {
                    printf("no superuser\n");
                    return 0;
                }


                fclose(pFile);
                free(pw);



                // group

                memset(&group, 0, sizeof(group));

                group.gr_name = login;
                group.gr_passwd = "*";
                group.gr_gid = passw.pw_gid;


                char* plogin[2];
                plogin[0] = login;
                plogin[1] = 0;


                group.gr_mem =  plogin;

                gFile = fopen ("/etc/group", "a");
                //gFile = fopen ("/home/alex/group", "a");

                int m = putgrent(&group, gFile);

                if ( m == -1)
                {
                    printf("no superuser\n");
                    return 0;
                }

                fclose(gFile);

                int status;
                if (status = stat(home, &st) == -1) // � ������, ���� ��� ������� ����������
                {
                     mkdir(home, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // ���� ����� ������ ������������
                     chown(home, passw.pw_uid, passw.pw_gid );

                     if (status = stat(home, &st) == -1)
                     {
                         printf("error with make directory(do from root)\n");
                         //return 0;
                     }

                }
            }

            return 0;
       }

       else
       {
           if (!strcmp(argv[1], "-h"))
           {
               printf("Usage: 3 [ARGS]\n"
                          "\t-h\t help\n"
                          "\t-f FILE\t from file\n"
                      "interactive format <login>;<fullname>\n"
                      "Alex Stiharnyy and Nikita Ytkin\n"
                      "KTYR-445\n");
               return 0;
           }

       }
   }













    return 0;
}
