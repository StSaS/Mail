#!/bin/bash

SYMBOLS=""
PASSWORD=""
RANDOM=256
n=0
l=0

if [ "$*" = "-h" ]
then
	echo "This programm generate  passwords"
	echo "-l <argument> - length passwords "
	echo "-n <argument> - quantity passwords"
	echo "Alex Stiharnyy && Nikita Ytkin"
	echo "	PS-445"
	exit
fi



if [ $# = 0 ];
then
	n=5
	l=10
else
	while getopts "l:n:" optname
  do
    case "$optname" in
      "l")
	if [ -z "${OPTARG//[0-9]/}" ]
	then
	l=$OPTARG
	else
	echo "invalid argument l"
	exit
	fi

	if [ $n = 0 ]
	then
	n=5
	fi
	        ;;
      "n")
        if [ -z "${OPTARG//[0-9]/}" ]
        then
        n=$OPTARG
        else
	echo "invalid argument n"
        exit
        fi
 
	if [ $l = 0 ]
        then
                l=10
        fi

        ;;
      "?")
       # echo "Unknown option $OPTARG"
	exit
        ;;
      ":")
        echo "No argument value for option $OPTARG"
	exit
        ;;
      "*")
      # ������������ �� �������
        >&2 echo "Unknown error while processing options"
	echo "Unknown error while processing options"
	exit
        ;;
    esac
  done
fi

# if n or l 0 return error 
for symbol in {A..Z} {a..z} {0..9}
do 
SYMBOLS=$SYMBOLS$symbol 
done

for k in `seq 1 $n`
do
	for i in `seq 1 $l`
	do
	PASSWORD=$PASSWORD${SYMBOLS:$(expr $RANDOM % ${#SYMBOLS}):1}
	done 
	echo $PASSWORD
	PASSWORD=""
done

