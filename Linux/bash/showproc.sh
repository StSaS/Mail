#!/bin/bash

name=0
memory=0
state=0

if [ $# -eq 0 ]
then
    memory=1
    name=1
    state=1
fi

if [ "$*" = "-h" ]

then

	echo "This programm demonstrate running processes"

	echo "-n | --name - name process "

	echo "-m | --memory - memory process"
	
	echo "-s | --state - state process"

	echo "Alex Stiharnyy && Nikita Ytkin"

	echo "	PS-445"

	exit

fi



for arg in "$@"; do
  shift
  case "$arg" in
    "--name") set -- "$@" "-n" ;;
    "--memory") set -- "$@" "-m" ;;
    "--state")   set -- "$@" "-s" ;;
    "-n") set -- "$@" "-n" ;;
    "-m") set -- "$@" "-m" ;;
    "-s")   set -- "$@" "-s" ;;
    *) echo "No reasonable options found!"; exit ;;
  esac
done


while getopts "nms" opt

do

case $opt in

n) name=1 ;;
m) memory=1 ;;
s) state=1 ;;
*) echo "No reasonable options found!"; exit ;;

esac

done

# Header

printf "%10s" "pid"

if [ $name -eq 1 ]
then
    printf "%20s" "name"
fi

if [ $memory -eq 1 ]
then
    printf "%10s" "memory"
fi

if [ $state -eq 1 ]
then
    printf "%25s" "state"
fi

echo
 
#

#echo "$(ls -dv /proc/*/ | grep -E '^/proc/[0-9]+/$' | grep -oE '[0-9]+')"
procs=($(ls -dv /proc/*/ | grep -E '^/proc/[0-9]+/$' | grep -oE '[0-9]+'))

for proc in "${procs[@]}"
do
   arr=($(cat "/proc/$proc/stat" 2>/dev/null)) 
  
   if [ ${#arr[@]} -ge 23 ]
   then
	pname=${arr[1]}
        pmemory=${arr[23]}
        pstate=${arr[2]}

        printf "%10d" $proc

        if [ $name -eq 1 ]
        then
            printf "%20s" $pname
        fi

        if [ $memory -eq 1 ]
        then		
            printf "%10dkb" $pmemory
        fi

        if [ $state -eq 1 ]
        then

	    if [ $pstate = "S" ]
            then
                printf "%25s" "interruptible sleep"
            fi
            
 	    if [ $pstate = "R" ]
            then
                printf "%25s" "running"
            fi

	    if [ $pstate = "D" ]
            then
                printf "%25s" "uninterruptible sleep"
            fi

	    if [ $pstate = "Z" ]
            then
                printf "%25s" "zombie"
            fi

	    if [ $pstate = "T" ]
            then
                printf "%25s" "stopped"
            fi

         fi

        echo
    fi
            
	     
done 