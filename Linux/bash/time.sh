#!/bin/bash


 
if [ "$*" = "-h" ]
then
        echo "This programm show time performance"
        echo "arguments - set of command "
        echo "Alex Stiharnyy && Nikita Ytkin Co."
        echo "  PS-445"
        exit
fi

if [ $# != 0 ]
then
	#command="$@"
	#"$command"
	#echo "$@"
	START=$(date +%s%N)
	"$@"
	END=$(date +%s%N)
else 
	echo "enter parametr"
	exit
fi
 
DIFF=$(( $END - $START ))
#DIFF=10850040003
DIVIDE=1000000000
#echo "$START"
#echo "$END" 
printf "It took %d.%2.9d seconds\n" $(($DIFF/$DIVIDE)) $(($DIFF%$DIVIDE))