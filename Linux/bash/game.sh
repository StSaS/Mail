#!/bin/bash

width=10
height=10
px=5
py=5
pixel=' '
check=' '
declare -a bitmap
tracer=1
# 1 - Up
# 2 - Right
# 3 - Down
# 4 - Left

random(){
count=0

px=$(($RANDOM % $(( $width - 3 )) + 1 )) 
py=$(($RANDOM % $(( $height - 3 )) + 1)) 

#echo "$px"
#echo "$py"

pixel="^"
set_bit $py $px


while [ "$count" -le 5 ]      # ��������� 10 ($MAXCOUNT) ��������� �����.
do
  rx=$(($RANDOM % $(($width -3)) + 1)) 
  ry=$(($RANDOM % $(($height -3)) + 1)) 
  
  if [ $rx -eq 1 ] && [ $ry -eq 1 ]
  then
       count=$(( $count - 1 ))
  else 
       pixel="o"
       set_bit $ry $rx
       count=$(( $count + 1 ))
       #echo "$ry"
       #echo "$rx"
  fi

done

}
mkblock(){

if [ $tracer -eq 1 ] &&  [ $py -gt 1 ] #up
then
	pyo=$(($py-1))
        pixel="o"
        set_bit $pyo $px

        draw $pyo $px
fi

if [ $tracer -eq 4 ] && [ $px -gt 1 ] #left
then
	pxo=$(($px-1))
        pixel="o"
        set_bit $py $pxo
        draw $py $pxo

fi

if [ $tracer -eq 3 ] &&  [ $py -lt $(( $height - 2 )) ] #down
then
	pyo=$(($py+1))
        pixel="o"
        set_bit $pyo $px
        draw $pyo $px

fi

if [ $tracer -eq 2 ]  &&  [ $px -lt $(( $width - 2 )) ] #right
then
	pxo=$(($px+1))
        pixel="o"
        set_bit $py $pxo
        draw $py $pxo

fi

}
 
rmblock(){
if [ $tracer -eq 1 ] &&  [ $py -gt 1 ] #up
then
        pyo=$(($py-1))
        pixel=" "
        set_bit $pyo $px

        draw $pyo $px
fi

if [ $tracer -eq 4 ] && [ $px -gt 1 ] #left
then
        pxo=$(($px-1))
        pixel=" "
        set_bit $py $pxo
        draw $py $pxo

fi

if [ $tracer -eq 3 ] &&  [ $py -lt $(( $height - 2 )) ] #down
then
        pyo=$(($py+1))
        pixel=" "
        set_bit $pyo $px
        draw $pyo $px

fi

if [ $tracer -eq 2 ]  &&  [ $px -lt $(( $width - 2 )) ] #right
then
        pxo=$(($px+1))
        pixel=" "
        set_bit $py $pxo
        draw $py $pxo

fi

}
function set_bit
{
    local i=$(($1 * $width + $2))
    bitmap[$i]=$pixel
}
 
function print_bitmap
{
   for (( i=0; i < $height; i++))
    do
        for (( j=0; j < $width; j++ ))
        do
            tput cup $i $j
            local index=$(($i * $width + $j))    
            echo  "${bitmap[$index]}"
        done
        
        echo
    done
}

BLUE() {
echo -en "\033c\033[0;1m\033[37;44m\033[J";}

BOARDS() {
for (( i=0; i < $height; i++))
do
    for (( j=0; j < $width; j++ ))
    do
      pixel=" "
      if [ $i -eq 0 ] || [ $i -ge $(($height - 1)) ] ||  [ $j -eq 0 ] || [ $j -ge $(($width - 1)) ]
      then
            pixel='x'
      fi
        
      set_bit $i $j
    done
done

#pixel="^" # default location
#set_bit 1 1
#pixel="o"
#set_bit 5 6
#set_bit 6 2
#set_bit 5 3
random
      
}

function draw(){

tput cup $1 $2
echo -en "$pixel"
}

up(){
local y=$(($py - 1))
get_bit $y $px

tracer=1

if [ "$py" -gt 1 ]  2> /dev/null  && [ "$check" != "o" ]
then
	pixel=' '
	set_bit $py $px

	draw $py $px
	
	py=$(($py-1))
	pixel="^"
	set_bit $py $px

	draw $py $px
else
 	pixel="^"
        set_bit $py $px

	draw $py $px
fi

#print_bitmap
        
}

left(){
local x=$(($px - 1))
get_bit $py $x

tracer=4

if [ "$px" -gt 1 ]  2> /dev/null  && [ "$check" != "o" ]
then
        pixel=' '
        set_bit $py $px

	draw $py $px

        px=$(($px-1))
        pixel="<"
        set_bit $py $px
	draw $py $px

else 
	pixel="<"
        set_bit $py $px
	draw $py $px

fi

#print_bitmap

}

down(){
local y=$(($py + 1))
get_bit $y $px

tracer=3

if [ "$py" -lt $(( $height - 2 )) ] 2> /dev/null  && [ "$check" != "o" ]
then
        pixel=' '
        set_bit $py $px
	draw $py $px


        py=$(($py+1))
        pixel="v"
        set_bit $py $px
	draw $py $px

else
        pixel="v"
        set_bit $py $px
	draw $py $px


fi

#print_bitmap

}

right(){
local x=$(($px + 1))
get_bit $py $x 

tracer=2

if [ "$px" -lt $(( $width - 2 )) ] 2> /dev/null  && [ "$check" != "o" ] 
then
        pixel=' '
        set_bit $py $px
	draw $py $px


        px=$(($px+1))
        pixel=">"
        set_bit $py $px
	draw $py $px

else
        pixel=">"
        set_bit $py $px
	draw $py $px

fi

#print_bitmap

}

function get_bit(){

local index=$(($1 * $width + $2))
check=${bitmap[$index]}
}

if [ $# = 0 ];

then

	height=10

	width=10

else
	while getopts "h:w:" optname

  	do

    		case "$optname" in

      				"h")

  		if [ -z "${OPTARG//[0-9]/}" ]

		then

		height=$OPTARG

		else

		echo "invalid argument h"

		exit

		fi
 			;;

		"w")
		
		 if [ -z "${OPTARG//[0-9]/}" ]

                then

                width=$OPTARG

                else

                echo "invalid argument w"

                exit

                fi
                        ;;

		 "?")

       		echo "Unknown option $OPTARG"

		exit

        	;;

      		":")

        	echo "No argument value for option $OPTARG"

		exit

        	;;

      		"*")

      		# ������������ �� �������

        	>&2 echo "Unknown error while processing options"

		echo "Unknown error while processing options"

		exit

        	;;

    	esac

    done

	
fi

clear

	echo "		GAME"
	echo 
	echo "Arguments:"
	echo "-w <argument> - width of map"
	echo "-h <argument> - height of map"
	echo
	echo "Control:"
        echo "w - UP"
	echo "a - Left"
	echo "d - Right"
	echo "s - Down"
	echo "m - Make block"
	echo "b - Destroy block"
	echo "q - quit"
	echo 
        echo "	Alex Stiharnyy && Nikita Ytkin Co."
	echo "  PS-445"
	echo
	echo "	   	PRESS ANY KEY"
 
read -n 1 #wait prees any key

BLUE    
BOARDS

clear
print_bitmap

stty -echo
tput civis


while :
do
    read -s -n 1 key
    case $key in
        w) up;;
        a) left;;
        s) down;;
        d) right;;
        m) mkblock;;
        b) rmblock;;
        q) stty echo; tput cnorm;  clear;  break;;

    esac
    
done
