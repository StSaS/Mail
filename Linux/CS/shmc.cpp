#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include <sys/fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <signal.h>
#include <QTextStream>

#define SEMNAMEW "/semaphoreW"
#define SEMNAMER "/semaphoreR"

key_t key_shm = 2001;
int bufSize=1024;
int blockSize=128;

sem_t* semW;
sem_t* semR;
void* shm;
pid_t other_pid;

void init(int* shmid, void** shm_addr);
void exit(void *shm_addr);
void transmit_file(void* shm, char *path);
void reset();
void signal_handler(int signo)
{
    if(signo == SIGINT)
        {
            printf("aborting\n");
            /*sem_close(semW);
            sem_close(semR);*/
            sem_unlink(SEMNAMEW); // sem_close
            sem_unlink(SEMNAMER);

            shmdt(shm);

            exit(1);
        }
}

int main(int argc, char** argv)
{
   char path[PATH_MAX];
   int shmid;
   //void* shm;
   if(argc > 1 && strcmp(argv[1], "-h") == 0)
          {
              printf("This programm to do copy file\n"
                     "Alex Stikharny\n"
                     "Nikita Ytkin\n"
                     "PS-345\n"); // print instructions

              return 0;
          }

   signal(SIGINT, signal_handler);

   if(argc >= 2)
           realpath(argv[1], path);
       else
       {
           printf("enter path\n");
           return 0;
       }

    //reset();
    init(&shmid, &shm);
    transmit_file(shm,path);
    exit(shm);



    return 0;
}

void init(int *shmid, void** shm_addr)
{
    semW = sem_open(SEMNAMEW, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, 0);
      if (semW == SEM_FAILED) perror("client: can not get semaphore");

      //провепка на 0 или -1
      int semval;
       sem_getvalue(semW, &semval);
      // if ((semval!=0)||(semval!=-1)) sem_init(semW, 0, 0);

    semR = sem_open(SEMNAMER, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, 7);
        if (semR == SEM_FAILED) perror("client: can not get semaphore");

        sem_getvalue(semR, &semval);
        //if ((semval!=7)) sem_init(semR, 0, 7);
      //установить в 7

    if ((*shmid =  shmget (key_shm,bufSize, IPC_CREAT | 0666)) < 0)
        perror("client: can not get shared memory segment");

    if ((*shm_addr = shmat (*shmid, 0, 0)) == NULL)
        perror("client: shared memory attach error");

    return;
}

void reset()
{
    int ret;
    int shmid;
    void* shm;


    ret = sem_unlink(SEMNAMEW);
    ret = sem_unlink(SEMNAMER);

    shmid =  shmget (key_shm,bufSize, IPC_CREAT | 0666);


    shm = shmat (shmid, 0, 0);


    shmdt(shm);
}

void exit(void* shm_addr)
{
    int ret,semval;

    do
    {
        sem_getvalue(semW, &semval);
       // printf("%d",semval);
    }
    while (semval>0);

    /*sem_close(semW);
    sem_close(semR);*/
    ret = sem_unlink(SEMNAMEW); // sem_close
    ret = sem_unlink(SEMNAMER);

    shmdt(shm_addr);

    return;
}

void transmit_file(void* shm, char* path)
{
    size_t size, left;
    struct stat info;
    int fd;
    int i, semval;
    int semlimit = bufSize / blockSize - 1;
    //printf("enter\n");

    fd = open(path, O_RDONLY);
    if (fd == -1) perror("client: can not open file");

    if (fstat(fd, &info) != -1)
        size = left = info.st_size;
        else perror("client: can not get stat");

    //printf("enter1\n");

    pid_t pid = getpid();

    memcpy(shm,(void*)&size,sizeof(size_t));
    memcpy((char*)shm + sizeof(size_t), (void*)&pid, sizeof(pid_t));

    //printf("enter2\n");
    sem_post(semW);// писать больше чем нужно//дать возможность читателю считать
    sem_wait(semR);
    /*printf("enter2\n");
    sem_wait(semR);*/

    /*printf("enter3\n");
    memcpy((void*)&other_pid, (char*)shm + sizeof(size_t), sizeof(pid_t));*/
    //sem_wait(semW);



    //printf("enter4\n");

    i = 1;
    //записать свой pid

        while(left > 0)
        {
            for(; i < bufSize / blockSize; i++)
            {
                if(left <= 0)
                    break;

                /*do
                {
                    sem_getvalue(sem, &semval);
                }while(semval >= semlimit);*/
                //sem_p(semR);
               // printf("%d\n",i);

                //printf("%d\n",i);


                int bytes = read(fd, (char*)shm + i * blockSize,
                        (left > blockSize) ? blockSize : left);
                left -= bytes;
                printf("%d\n",left);
                sem_post(semW);//дать писателю возможностьсчитать новую инфу
                sem_wait(semR);//не дать возможность отрваться от читателя
            }

            i = 0;
        }
        /*sem_getvalue(semW, &semval);
        printf("%d\n",semval);*/

        close(fd);

    //write in buffer from file biginwith 2 index

    /*QTextStream cin(stdin);

    int k;*/
   // cin>>k;

    //later
}