#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <linux/limits.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <time.h>

#define DIRFLAG 1
#define ENDFLAG 1 << 1
#define ERRFLAG 1 << 2

struct info
{
    size_t data_len;
    time_t mod_time;
    size_t size;
    int flags;
};

int readn(int fd, void* buf, size_t len)
{
    size_t total_read = 0, total_left = len;
    char* ptr = buf;
    ssize_t bytes = read(fd, (void*)ptr, total_left);

    while(total_left > 0)
    {
        if(bytes <= 0)
            return 1;

        total_read += bytes;
        total_left -= bytes;
        ptr += bytes;
    }
    return 0;
}

void client(int* cs_fd, int* sc_fd, char* path)
{
    struct info entry_info;
    int fname_max = 256;
    char fname[fname_max];
    char time_str[20];
    char mark = 'f';

    close(cs_fd[0]);
    close(sc_fd[1]);

    write(cs_fd[1], path, strlen(path) + 1);

    while(1)
    {
        read(sc_fd[0], (void*)&entry_info, sizeof(struct info));
        
        if(entry_info.flags & ERRFLAG)
        {
            read(sc_fd[0], (void*)&errno, sizeof(errno));
            perror("error: ");
            if(entry_info.flags & ENDFLAG)
                break;
            else
                continue;
        }

        if(entry_info.flags & ENDFLAG)
            break;

        read(sc_fd[0], (void*)fname, entry_info.data_len);

        strftime(time_str, 20, "%Y-%m-%d %H:%M:%S", localtime(&entry_info.mod_time));

        if((entry_info.flags & DIRFLAG) != 0)
            mark = 'd';
        
        printf("%c %s %lu bytes\t%s\n", mark, time_str, entry_info.size, fname);
    }

}

void server(int* cs_fd, int* sc_fd)
{
    int bytes_read;
    char path[PATH_MAX];
    struct stat entry_stat;
    struct dirent entry;
    struct info entry_info;
    DIR* dir;
    struct dirent* pd;

    close(cs_fd[1]);
    close(sc_fd[0]);

    bytes_read = read(cs_fd[0], path, PATH_MAX);
    
    dir = opendir(path);
    if(dir == NULL)
    {
        entry_info.flags = ENDFLAG | ERRFLAG;
        entry_info.data_len = sizeof(errno);
        write(sc_fd[1], (void*)&entry_info, sizeof(struct info));
        write(sc_fd[1], (void*)&errno, sizeof(errno)); 
        return;
    }

    while(1)
    {
        errno = 0;
        pd = readdir(dir);
        if(pd == NULL)
        {
            if(errno == 0)
            {
                entry_info.flags = ENDFLAG;
                write(sc_fd[1], (void*)&entry_info, sizeof(struct info));
                break;
            }

            entry_info.flags = ERRFLAG;
            entry_info.data_len = sizeof(errno);
            write(sc_fd[1], (void*)&entry_info, sizeof(struct info));
            write(sc_fd[1], (void*)&errno, sizeof(errno)); 

            continue;
        }

        memcpy((void*)&entry, (void*)pd, sizeof(struct dirent));
        stat(entry.d_name, &entry_stat);

        entry_info.mod_time = entry_stat.st_mtime;
        entry_info.data_len = strlen(entry.d_name) + 1;
        entry_info.size = entry_stat.st_size;
        entry_info.flags = (entry_stat.st_mode & S_IFDIR) == 0 ? 0 : DIRFLAG;
       
        write(sc_fd[1], (void*)&entry_info, sizeof(struct info));
        write(sc_fd[1], entry.d_name, entry_info.data_len);
    }
    
    closedir(dir);
}

int main(int argc, char** argv)
{
    char dir[PATH_MAX];
    int cs_fd[2], sc_fd[2];
    int pid;

    if(argc > 1 && strcmp(argv[1], "-h") == 0)
    {
        printf("usage: 2 [path]\n");
        return 0;
    }
    
    if(argc == 1)
    {
        if(getcwd(dir, PATH_MAX) == NULL)
        {
            perror("getcwd()");
            return 1;
        }
    }
    else
        memcpy((void*)dir, argv[1], strlen(argv[1]));

    chdir(dir);

    pipe(cs_fd);
    pipe(sc_fd);

    if((pid = fork()) == -1)
    {
        perror("fork()");
        return 1;
    }

    if(pid == 0)
        server(cs_fd, sc_fd);
    else
        client(cs_fd, sc_fd, dir);

    return 0;
}
