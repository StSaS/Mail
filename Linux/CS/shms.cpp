#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include <sys/fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <signal.h>

#define SEMNAMEW "/semaphoreW"
#define SEMNAMER "/semaphoreR"

key_t key_shm = 2001;
int bufSize=1024;
int blockSize=128;

sem_t* semW;
sem_t* semR;
void* shm;
pid_t other_pid;

void init(int* shmid, void** shm_addr);
void exit(void *shm_addr);
void receive_file(void* shm, char* path); // add path

void signal_handler(int signo)
{
    if(signo == SIGINT)
        {
            printf("aborting\n");
            //exit(shm);
            int ret,semval;



            ret = sem_unlink(SEMNAMEW);
            ret = sem_unlink(SEMNAMER);

            sem_close(semW);
            sem_close(semR);

            shmdt(shm);

            exit(1);
        }
}

int main(int argc, char** argv)
{
   char path[PATH_MAX];
   int shmid;
   //void* shm;
   if(argc > 1 && strcmp(argv[1], "-h") == 0)
          {
              printf("This programm to do copy file\n"
                     "Alex Stikharny\n"
                     "Nikita Ytkin\n"
                     "PS-345\n"); // print instructions

              return 0;
          }

   signal(SIGINT, signal_handler);

   if(argc >= 2)
           realpath(argv[1], path);
       else
       {
           printf("enter path\n");
           return 0;
       }

    init(&shmid, &shm);
    receive_file(shm,path);
    exit(shm);

    return 0;
}


void init(int *shmid, void** shm_addr)
{
    semW = sem_open(SEMNAMEW, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, 0);//на новую инфу
      if (semW == SEM_FAILED) perror("client: can not get semaphore");//на отрыв

      int semval;
       sem_getvalue(semW, &semval);
      // if ((semval!=0)||(semval!=1)) sem_init(semW, 0, 0);

    semR = sem_open(SEMNAMER, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, 7);
        if (semR == SEM_FAILED) perror("client: can not get semaphore");

        sem_getvalue(semR, &semval);
        //if ((semval>7)) sem_init(semR, 0, 7);

    if ((*shmid =  shmget (key_shm,bufSize, 0666)) < 0)
        perror("server: can not get shared memory segment");

    if ((*shm_addr = shmat (*shmid, 0, 0)) == NULL)
        perror("server: shared memory attach error");

    return;
}

void exit(void* shm_addr)
{
    int ret,semval;



    ret = sem_unlink(SEMNAMEW);
    ret = sem_unlink(SEMNAMER);

    sem_close(semW);
    sem_close(semR);

    shmdt(shm_addr);

    return;
}

void receive_file(void* shm, char *path)
{
        size_t size, left;
        int fd, i, semval;
        pid_t pid = getpid();

        fd = open(path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
        if (fd == -1) perror("server: can not open file");

        sem_getvalue(semW, &semval);
        printf("%d\n",semval);
        sem_wait(semW);

        //printf("norm");
        memcpy((void*)&size, shm, sizeof(size_t));
        left=size;
        memcpy((void*)&other_pid, (char*)shm + sizeof(size_t), sizeof(pid_t));

        //memcpy((char*)shm + sizeof(size_t), (void*)&pid, sizeof(pid_t));

        //sem_post(semW);
        sem_post(semR);

        //read from 1 index
        i = 1;
            while(left > 0)
            {
                for(; i < bufSize / blockSize; i++)
                {
                    if(left <= 0)
                        break;

                    /*do
                    {
                        sem_getvalue(sem, &semval);
                    }while(semval >= semlimit);*/
                    //sem_p(semR);
                    sem_wait(semW);//ждать пока у писатель запишет новую инфу



                    int bytes = write(fd, (char*)shm + i * blockSize,
                            (left > blockSize) ? blockSize : left);
                    left -= bytes;
                    //printf("%d\n",left);
                    printf("%d\n",left);
                    sem_post(semR);//дать писателю возможность отроваться
                }

                i = 0;
            }

            close(fd);
       /* memcpy((void*)&pid,shm, sizeof(pid_t));
        memcpy((void*)&size,(char*)shm + sizeof(pid_t),sizeof(size_t));*/

        //sem_post(sem);
        //printf("%d\n",pid);
        //printf("%d",size);



    //later
}